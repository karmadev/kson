# KSON
KarmaDev Json, or KSON to abbreviate, is a java library
which helps in parsing and reading json objects. It also
allows to serialize objects directory to json and load
them later, all by using annotations

# Keys, paths and path separators
In Kson, every json element is conscious of who he is,
this means that every object knows in which key he is
assigned to, for instance, a number "5" in {"key": 5}
knows that he's assigned under "key". To accomplish that,
you must give the name to every element but the first
or "main" one. You can also specify a element path, which
is the route to follow to get into an element, and a
path separator when navigating.

This is all because in Kson, you can retrieve any child
item of an object directly, without iterating nor doing
multiple `getAsObject()`. That's what the path is for and
the path separator. For instance, on a json file like this:

```json
{
  "co1": {
    "co2": {
      "co3": {
        "key": "value"
      }
    }
  }
}
```

You could just do the following:

```java
import com.github.karmadeb.kson.element.JsonElement;
import com.github.karmadeb.kson.element.JsonObject;
import com.github.karmadeb.kson.parser.JsonParser;

void main() {
    JsonParser parser = JsonParser.create(exampleJson);
    JsonElement element = parser.resolve();

    if (element.isObject()) {
        JsonObject object = element.getAsObject();
        String value = object.get("co1.co2.co3.key");
        
        System.out.println(value);
    }
}
```

# API Example

### Reading a json string
When reading a json, you can read from a `Path`, `File`,
`InputStream`, `String` and `byte[]`. In all cases a valid
json is expected, in case of bytes, it would be
the byte representation of a json string

```java

import com.github.karmadeb.kson.element.JsonElement;
import com.github.karmadeb.kson.parser.JsonParser;

private static final String exampleJson = "{\"key\": \"value\"}";

void main() {
    JsonParser parser = JsonParser.create(exampleJson);
    JsonElement element = parser.resolve();

    int indentation = 1;
    char indentationCharacter = '\t';
    System.out.println(element.toJsonString(indentation, indentationCharacter));
    /*
    Also works with:
    
    System.out.println(element.toJsonString(indentation));
    System.out.println(element.toJsonString());
     */
}
```

### Writing a json
Writing a json is very easy, once you know the schema
your json will follow.

```java

import com.github.karmadeb.kson.element.JsonObject;

void main() {
    JsonObject object = new JsonObject();
    JsonObject child1 = new JsonObject();
    JsonObject child2 = new JsonObject();
    JsonObject child3 = new JsonObject();

    object.put("co1", ob1);
    child1.put("co2", ob2);
    child2.put("co3", ob3);

    child3.put("key", "value");
    
    String raw = object.toJsonString();
}
```

# Serializing
Kson allows to serialize a class very easily, in a
json format and load it later, without having to implement
anything _except for transformers for special fields_.

```java

import com.github.karmadeb.kson.annotation.serialization.JsonExclude;
import com.github.karmadeb.kson.annotation.serialization.JsonInclude;
import com.github.karmadeb.kson.annotation.serialization.JsonSerializable;

import java.security.SecureRandom;

@JsonSerializable(serialId = 1024)
public final class Customer {

    @JsonExclude
    private final long RUNTIME_ID = new SecureRandom().nextInt();

    @JsonInclude
    private final long id;

    @JsonInclude
    private final String name;

    @JsonInclude
    private final int age;
    
    @JsonInclude(name = "email_address")
    private final String emailAddress;
}
```

Kson will try to map then the values, by using reflection, but it's highly
recommended to specify a constructor **which must match the serialId**

```java
import com.github.karmadeb.kson.annotation.deserialization.JsonConstructor;
import com.github.karmadeb.kson.annotation.deserialization.JsonParameter;

@JsonSerializable(serialId = 1024)
public final class Customer {
    
    ...

    public Customer() {
        this(1, "name", 1, "example@domain.com");
    }
    
    @JsonConstructor(serialId = 1024)
    private Customer(final @JsonParameter(fieldName = "id") long id,
                    final @JsonParameter(fieldName = "name") String name,
                    final @JsonParameter(fieldName = "age") int age,
                    final @JsonParameter(fieldName = "email_address") String emailAddress) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.emailAddress = emailAddress;
    }
}
```

If you change someday your class fields, by adding or removing them, it's highly
recommended to change the `JsonSerializable` serialId, and create a new constructor
with the `JsonConstructor` and new serialId, in order to provide both constructor
support for old serialized customers, and new serialized customers, even though
Kson will try to inject the field values through reflection if they are not provided
in the constructor or there's no constructor with serial id

```java

import com.github.karmadeb.kson.element.JsonElement;
import com.github.karmadeb.kson.printer.JsonPrinter;
import com.github.karmadeb.kson.reflection.transformer.TransformerUtils;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

void main() throws IOException {
    Customer customer = new Customer();
    //Let's imagine the empty constructor initializes the customer fields to all "1"
    JsonElement element = TransformerUtils.transform(customer);

    Path targetFile = Paths.get(customer.getId() + ".json");
    try (Writer writer = Files.newBufferedWriter(targetFile)) {
        JsonPrinter<?> printer = element.getJsonPrinter()
                .withIndent(1)
                .withIndentChar('\t');
        
        printer.print(1, writer);
    }
}
```

```java
import com.github.karmadeb.kson.element.JsonElement;
import com.github.karmadeb.kson.parser.JsonParser;
import com.github.karmadeb.kson.reflection.transformer.TransformerUtils;

void main() {
    Path sourceFile = Paths.get(customer.getId() + ".json");
    JsonParser parser = JsonParser.create(sourceFile);

    JsonElement element = parser.resolve();
    Customer customer = (Customer) TransformerUtils.resolve(element);
}

```