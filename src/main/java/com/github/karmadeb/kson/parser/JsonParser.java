package com.github.karmadeb.kson.parser;

import com.github.karmadeb.kson.element.JsonArray;
import com.github.karmadeb.kson.element.JsonElement;
import com.github.karmadeb.kson.element.JsonObject;
import com.github.karmadeb.kson.element.JsonPrimitive;
import com.github.karmadeb.kson.exception.JsonMalformedException;
import com.github.karmadeb.kson.exception.JsonParseException;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Represents a json parser
 */
public final class JsonParser {

    private final byte[] data;

    /**
     * Creates the json parser
     * @param data the json data
     */
    private JsonParser(final byte[] data) {
        this.data = data;
    }

    /**
     * Resolve the element
     * @return the resolved element
     * @throws JsonMalformedException if the json is malformed
     */
    public JsonElement resolve() throws JsonMalformedException {
        String raw = new String(this.data).trim();
        String[] rawData = raw.split("\n");

        boolean isArray = raw.startsWith("[");

        char expectedEnd = '}';
        if (isArray) {
            expectedEnd = ']';
        } else if (!raw.startsWith("{")) {
            throw new JsonMalformedException("Expected [ or {, but got " + (!raw.isEmpty() ? raw.charAt(0) : '\0'));
        }

        String lastLine = rawData[rawData.length - 1];
        if (raw.charAt(raw.length() - 1) != expectedEnd)
            throw new JsonMalformedException("Expected " + expectedEnd + " but got " + (!lastLine.isEmpty() ? lastLine.charAt(lastLine.length() - 1) : '\0'));

        raw = raw.substring(1, raw.length() - 1).trim();
        if (isArray) {
            JsonArray array = new JsonArray();
            parseElement(raw.replace("\n", ""), array);

            return array;
        } else {
            JsonObject object = new JsonObject();
            parseElement(raw.replace("\n", ""), object);

            return object;
        }
    }

    @SuppressWarnings("t")
    private void parseElement(final String data, final JsonElement output) {
        StringBuilder objectBuilder = new StringBuilder();
        StringBuilder keyBuilder = new StringBuilder();
        char[] characters = data.toCharArray();

        char objectOpen = '\0';
        char expectedEnd = '\0';
        int objectIndex = 0;
        boolean hasKeyAndValue = false;
        for (int i = 0; i < characters.length; i++) {
            char character = characters[i];
            if (character == ':' || character == ',' || Character.isSpaceChar(character)
                    || Character.isWhitespace(character)) {
                if (objectIndex > 0) {
                    objectBuilder.append(character);
                    continue;
                }

                if (output.isObject() && character == ':') {
                    keyBuilder.append(objectBuilder);
                    objectBuilder.setLength(0);
                    continue;
                }

                hasKeyAndValue = true;
                if (character == ',') {
                    tryMakeObject(output, keyBuilder, objectBuilder);
                    hasKeyAndValue = false;
                }

                continue;
            }

            if (character == '[' || character == '{') {
                if (expectedEnd == '\0') {
                    objectOpen = character;
                    expectedEnd = (character == '[' ? ']' : '}');
                    objectIndex += 1;
                } else {
                    if (objectOpen == character)
                        objectIndex += 1;
                }
            } else if (character == expectedEnd) {
                objectIndex -= 1;
                if (objectIndex == 0)
                    expectedEnd = '\0';
            }

            i = handleNewCharacter(data, character, i, characters, objectBuilder);
            if (keyBuilder.length() != 0)
                hasKeyAndValue = true;
        }

        if (hasKeyAndValue || output.isArray())
            tryMakeObject(output, keyBuilder, objectBuilder);
    }

    private static int handleNewCharacter(final String data, final char character, int i, final char[] characters, final StringBuilder objectBuilder) {
        if (character == '"') {
            boolean escaped;
            int newIndex;

            do {
                newIndex = data.indexOf('"', i + 1);
                if (newIndex == -1 || newIndex == i)
                    throw new JsonMalformedException("Unterminated string");

                escaped = characters[newIndex - 1] == '\\';
            } while (escaped);

            objectBuilder.append(data, i, newIndex + 1);
            i = newIndex;
        } else {
            objectBuilder.append(character);
        }

        return i;
    }

    private void tryMakeObject(final JsonElement output, final StringBuilder key, final StringBuilder objectBuilder) {
        JsonElement parsed;
        String raw = objectBuilder.toString().replace("\\/", "/");
        objectBuilder.setLength(0);

        if (raw.equalsIgnoreCase("true") || raw.equalsIgnoreCase("false")) {
            parsed = new JsonPrimitive(raw.equalsIgnoreCase("true"));
        } else if (raw.startsWith("{") || raw.startsWith("[")) {
            JsonParser parser = JsonParser.create(raw);
            parsed = parser.resolve();
        } else if (raw.startsWith("\"")) {
            parsed = new JsonPrimitive(raw.substring(1, raw.length() - 1));
        } else {
            JsonPrimitive rawPrimitive = new JsonPrimitive(raw);

            try {
                parsed = new JsonPrimitive(rawPrimitive.getAsNumber());
            } catch (NumberFormatException ex) {
                parsed = rawPrimitive;
            }
        }

        if (output.isArray()) {
            output.getAsArray().add(parsed);
        } else {
            output.getAsObject().set(key.substring(1, key.length() - 1), parsed);
            key.setLength(0);
        }
    }

    /**
     * Create a json parser for the
     * specified json data
     *
     * @param data the json data
     * @return the json parser
     * @throws JsonParseException if the data fails to parse
     */
    public static JsonParser create(final Path data) throws JsonParseException {
        try (InputStream stream = Files.newInputStream(data)) {
            return create(stream);
        } catch (IOException ex) {
            throw new JsonParseException("Failed to parse " + data, ex);
        }
    }

    /**
     * Create a json parser for the
     * specified json data
     *
     * @param data the json data
     * @return the json parser
     * @throws JsonParseException if the data fails to parse
     */
    public static JsonParser create(final File data) throws JsonParseException {
        try (FileInputStream stream = new FileInputStream(data)) {
            return create(stream);
        } catch (IOException ex) {
            throw new JsonParseException("Failed to parse " + data, ex);
        }
    }

    /**
     * Create a json parser for the
     * specified json data
     *
     * @param data the json data
     * @return the json parser
     * @throws JsonParseException if the data fails to parse
     */
    public static JsonParser create(final InputStream data) throws JsonParseException {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            byte[] buffer = new byte[2048];
            int read;

            while ((read = data.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }

            return create(out.toByteArray());
        } catch (IOException ex) {
            throw new JsonParseException("Failed to parse data stream", ex);
        }
    }

    /**
     * Create a json parser for the
     * specified json data
     *
     * @param data the json data
     * @return the json parser
     */
    public static JsonParser create(final String data) {
        return create(data.getBytes());
    }

    /**
     * Create a json parser for the
     * specified json data
     *
     * @param data the json data
     * @return the json parser
     */
    public static JsonParser create(final byte[] data) {
        return new JsonParser(data);
    }
}