package com.github.karmadeb.kson.element;

import com.github.karmadeb.kson.printer.JsonPrinter;
import com.github.karmadeb.kson.printer.type.JsonObjectPrinter;

import java.util.*;

/**
 * Represents a json object
 */
@SuppressWarnings("unused")
public final class JsonObject extends JsonElement implements Iterable<JsonElement> {

    private final JsonPrinter<JsonObject> printer = new JsonObjectPrinter(this);
    private final Map<String, JsonElement> elementMap = new LinkedHashMap<>();

    /**
     * Creates an empty json object
     */
    public JsonObject() {

    }

    /**
     * Creates a json object cloning
     * the specified object elements
     *
     * @param clone the clone to read from
     */
    public JsonObject(final JsonObject clone) {
        this(clone.elementMap);
        this.setParent(clone.parent);
        this.setKey(clone.key);
    }

    /**
     * Creates a json object from the
     * specified element map
     *
     * @param elementMap the object elements
     */
    public JsonObject(final Map<String, JsonElement> elementMap) {
        Map<String, JsonElement> clone = new HashMap<>();
        elementMap.forEach((key, value) -> clone.put(key, value.copy()));

        this.elementMap.putAll(clone);
        mapElementsData();
    }

    /**
     * Get the element size. The size
     * represents the amount of key -> values
     * this element has
     *
     * @return the element size
     */
    @Override
    public int size() {
        return this.elementMap.size();
    }

    /**
     * Get the element json printer
     *
     * @return the json printer of the
     * element
     */
    @Override
    public JsonPrinter<JsonObject> getJsonPrinter() {
        return this.printer;
    }

    /**
     * Copy the element into a new
     * element
     */
    @Override
    public JsonObject copy() {
        return new JsonObject(this);
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    @Override
    public Iterator<JsonElement> iterator() {
        return this.elementMap.values().iterator();
    }

    /**
     * Put an element into the object
     * @param key the element key
     * @param value the element
     * @return the previous element
     */
    public JsonElement set(final String key, final Boolean value) {
        return this.putElement(key, value);
    }

    /**
     * Put an element into the object
     * @param key the element key
     * @param value the element
     * @return the previous element
     */
    public JsonElement set(final String key, final Number value) {
        return this.putElement(key, value);
    }

    /**
     * Put an element into the object
     * @param key the element key
     * @param value the element
     * @return the previous element
     */
    public JsonElement set(final String key, final CharSequence value) {
        return this.putElement(key, value);
    }

    /**
     * Put an element into the object
     * @param key the element key
     * @param element the element
     * @return the previous element
     */
    public JsonElement set(final String key, final JsonElement element) {
        return this.putElement(key, element);
    }

    /**
     * Get an element in the object
     *
     * @param key the object key
     * @return the element
     */
    public JsonElement get(final String key) {
        JsonElement element = this.find(key);
        if (element == null) {
            element = JsonPrimitive.createNull();
            element.setParent(this);
            element.setKey(key);
        }

        return element;
    }

    /**
     * Get an element in the object
     *
     * @param key the object key
     * @param def the object default
     * @return the element
     */
    public JsonElement getOrDefault(final String key, final JsonElement def) {
        JsonElement element = this.find(key);
        if (element == null)
            element = def;

        if (element == null)
            element = JsonPrimitive.createNull();

        if (!element.isChildOf(this) || !element.getKey().equals(key)) {
            element.setParent(this);
            element.setKey(key);
        }

        return element;
    }

    private JsonElement find(final String key) {
        String parsed = key.replace("\\.", "%literal_dot%");
        if (parsed.contains(".")) {
            String[] navTree = parsed.split("\\.");
            JsonObject element = this;
            for (int i = 0; i < navTree.length - 1; i++) {
                String path = navTree[i];
                JsonElement jElement = element.elementMap.get(path
                        .replace("%literal_dot%", "."));

                if (jElement == null || !jElement.isObject()) return null;
                element = jElement.getAsObject();
            }

            return element.elementMap.get(navTree[navTree.length - 1]
                    .replace("%literal_dot%", "."));
        }

        return this.elementMap.get(parsed.replace("%literal_dot%", "."));
    }

    /**
     * Remove a key from the
     *
     * @param key the key to remove
     * @return the removed element
     */
    public JsonElement remove(final String key) {
        String parsed = key.replace("\\.", "%literal_dot%");
        if (parsed.contains(".")) {
            String[] navTree = parsed.split("\\.");
            JsonObject element = this;
            for (int i = 0; i < navTree.length - 1; i++) {
                String path = navTree[i];
                JsonElement jElement = element.elementMap.get(path
                        .replace("%literal_dot%", "."));

                if (jElement == null || !jElement.isObject()) return null;
                element = jElement.getAsObject();
            }

            return element.elementMap.remove(navTree[navTree.length - 1]
                    .replace("%literal_dot%", "."));
        }

        return this.elementMap.remove(key);
    }

    /**
     * Get if the object has the specified
     * key
     *
     * @param key the key
     * @return if the object has the key
     */
    public boolean has(final String key) {
        return this.elementMap.containsKey(key);
    }

    /**
     * Get the object keys
     *
     * @return the keys of the object
     */
    public Collection<String> getKeys() {
        return Collections.unmodifiableCollection(this.elementMap.keySet());
    }

    /**
     * Get the object values
     *
     * @return the values of the object
     */
    public Collection<JsonElement> getElements() {
        return Collections.unmodifiableCollection(this.elementMap.values());
    }

    /**
     * Get a set with an entry of object key -> value
     * of this object
     *
     * @return the object values entry set
     */
    public Set<Map.Entry<String, JsonElement>> getEntries() {
        return this.elementMap.entrySet();
    }

    /**
     * Get the object as a map
     *
     * @return the object map
     */
    public Map<String, JsonElement> asMap() {
        return Collections.unmodifiableMap(this.elementMap);
    }

    private void mapElementsData() {
        for (Map.Entry<String, JsonElement> entry : this.elementMap.entrySet()) {
            String key = entry.getKey();
            JsonElement element = entry.getValue();
            if (element.isChildOf(this) && element.getKey().equals(key))
                continue;

            if (element == null) {
                element = JsonPrimitive.createNull();
                entry.setValue(element);
            }

            element.setParent(this);
            element.setKey(key);
        }
    }

    private JsonElement putElement(final String key, final Object value) {
        JsonElement valueElement;

        if (value instanceof Boolean) {
            valueElement = new JsonPrimitive((Boolean) value);
        } else if (value instanceof Number) {
            valueElement = new JsonPrimitive((Number) value);
        } else if (value instanceof CharSequence) {
            valueElement = new JsonPrimitive((CharSequence) value);
        } else if (value instanceof JsonElement) {
            valueElement = (JsonElement) value;
        } else {
            valueElement = JsonPrimitive.createNull();
        }

        valueElement.setParent(this);
        valueElement.setKey(key);

        String parsed = key.replace("\\.", "%literal_dot%");
        if (parsed.contains(".")) {
            String[] navTree = parsed.split("\\.");
            JsonObject element = this;
            for (int i = 0; i < navTree.length - 1; i++) {
                String path = navTree[i];
                JsonElement jElement = element.elementMap.get(path
                        .replace("%literal_dot%", "."));

                if (jElement == null) {
                    jElement = new JsonObject();
                    element.set(path.replace("%literal_dot%", "."), jElement);
                }

                if (!jElement.isObject()) return null;
                element = jElement.getAsObject();
            }

            return element.elementMap.put(navTree[navTree.length - 1]
                    .replace("%literal_dot%", "."), valueElement);
        }

        return this.elementMap.put(key, valueElement);
    }
}
