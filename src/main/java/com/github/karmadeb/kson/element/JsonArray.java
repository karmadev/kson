package com.github.karmadeb.kson.element;

import com.github.karmadeb.kson.printer.JsonPrinter;
import com.github.karmadeb.kson.printer.type.JsonArrayPrinter;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Represents a json array
 */
@SuppressWarnings("unused")
public final class JsonArray extends JsonElement implements Iterable<JsonElement> {

    private final JsonPrinter<JsonArray> printer = new JsonArrayPrinter(this);
    private final List<JsonElement> elements = new ArrayList<>();

    /**
     * Creates an empty json array
     */
    public JsonArray() {

    }

    /**
     * Creates a cloned json array
     *
     * @param clone the array to clone from
     */
    public JsonArray(final JsonArray clone) {
        this(clone.elements);
        this.setParent(clone.parent);
        this.setKey(clone.key);
    }

    /**
     * Creates a mapped json array
     *
     * @param elements the initial elements
     */
    public JsonArray(final Collection<? extends JsonElement> elements) {
        this.elements.addAll(Objects.requireNonNull(elements).stream()
                .map((element) -> {
                    JsonElement collElement = element.copy();
                    if (collElement == null)
                        collElement = JsonPrimitive.createNull();

                    collElement.setParent(this);
                    return collElement;
                })
                .collect(Collectors.toList()));
    }

    /**
     * Get the element size. The size
     * represents the amount of key -> values
     * this element has
     *
     * @return the element size
     */
    @Override
    public int size() {
        return this.elements.size();
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    @Override
    public Iterator<JsonElement> iterator() {
        return this.elements.iterator();
    }

    /**
     * Performs the given action for each element of the {@code Iterable}
     * until all elements have been processed or the action throws an
     * exception.  Unless otherwise specified by the implementing class,
     * actions are performed in the order of iteration (if an iteration order
     * is specified).  Exceptions thrown by the action are relayed to the
     * caller.
     *
     * @param action The action to be performed for each element
     * @throws NullPointerException if the specified action is null
     * @implSpec <p>The default implementation behaves as if:
     * <pre>{@code
     *     for (T t : this)
     *         action.accept(t);
     * }</pre>
     * @since 1.8
     */
    @Override
    public void forEach(final Consumer<? super JsonElement> action) {
        this.elements.forEach(action);
    }

    /**
     * Creates a {@link Spliterator} over the elements described by this
     * {@code Iterable}.
     *
     * @return a {@code Spliterator} over the elements described by this
     * {@code Iterable}.
     * @implSpec The default implementation creates an
     * <em><a href="Spliterator.html#binding">early-binding</a></em>
     * spliterator from the iterable's {@code Iterator}.  The spliterator
     * inherits the <em>fail-fast</em> properties of the iterable's iterator.
     * @implNote The default implementation should usually be overridden.  The
     * spliterator returned by the default implementation has poor splitting
     * capabilities, is unsized, and does not report any spliterator
     * characteristics. Implementing classes can nearly always provide a
     * better implementation.
     * @since 1.8
     */
    @Override
    public Spliterator<JsonElement> spliterator() {
        return this.elements.spliterator();
    }

    /**
     * Get if this element is a
     * json array
     *
     * @return if the element is
     * an array
     */
    @Override
    public boolean isArray() {
        return true;
    }

    /**
     * Get the element json printer
     *
     * @return the json printer of the
     * element
     */
    @Override
    public JsonPrinter<JsonArray> getJsonPrinter() {
        return this.printer;
    }

    /**
     * Copy the element into a new
     * element
     */
    @Override
    public JsonArray copy() {
        return new JsonArray(this);
    }

    /**
     * Adds a boolean to the array
     *
     * @param value the value
     */
    public void add(final Boolean value) {
        JsonElement element = (value == null ? JsonPrimitive.createNull() : new JsonPrimitive(value));
        element.setParent(this);

        this.add(element);
    }

    /**
     * Adds a number to the array
     *
     * @param number the number
     */
    public void add(final Number number) {
        JsonElement element = (number == null ? JsonPrimitive.createNull() : new JsonPrimitive(number));
        element.setParent(this);

        this.add(element);
    }

    /**
     * Adds a char sequence to the array
     *
     * @param sequence the char sequence
     */
    public void add(final CharSequence sequence) {
        JsonElement element = (sequence == null ? JsonPrimitive.createNull() : new JsonPrimitive(sequence));
        element.setParent(this);

        this.add(element);
    }

    /**
     * Adds an element to the array
     *
     * @param element the element
     */
    public void add(JsonElement element) {
        if (element == null)
            element = JsonPrimitive.createNull();

        element.setParent(this);
        this.elements.add(element);
    }

    /**
     * Add all the elements from the
     * specified json array into this
     * array
     *
     * @param other the array to read from
     */
    public void addAll(final JsonArray other) {
        this.elements.addAll(other.elements);
    }

    /**
     * Add all the elements from the
     * specified json collection into this
     * array
     *
     * @param elements the collection to read from
     */
    public void addAll(final Collection<? extends JsonElement> elements) {
        this.elements.addAll(elements.stream()
                .map((e) -> {
                    JsonElement collElement = e;
                    if (collElement == null)
                        collElement = JsonPrimitive.createNull();

                    collElement.setParent(this);
                    return collElement;
                }).collect(Collectors.toList()));
    }

    /**
     * Get if the array contains the
     * specified element
     *
     * @param bool the element
     * @return if the array contains the element
     */
    public boolean contains(final Boolean bool) {
        return this.elements.contains((bool == null ? JsonPrimitive.createNull() : new JsonPrimitive(bool)));
    }

    /**
     * Get if the array contains the
     * specified element
     *
     * @param number the element
     * @return if the array contains the element
     */
    public boolean contains(final Number number) {
        return this.elements.contains((number == null ? JsonPrimitive.createNull() : new JsonPrimitive(number)));
    }

    /**
     * Get if the array contains the
     * specified element
     *
     * @param sequence the element
     * @return if the array contains the element
     */
    public boolean contains(final CharSequence sequence) {
        return this.elements.contains((sequence == null ? JsonPrimitive.createNull() : new JsonPrimitive(sequence)));
    }

    /**
     * Get if the array contains the
     * specified element
     *
     * @param element the element
     * @return if the array contains the element
     */
    public boolean contains(final JsonElement element) {
        return this.elements.contains(element);
    }

    /**
     * Set the element at the specified index
     *
     * @param index the element index
     * @param newElement the new element
     * @return the previous element
     */
    public JsonElement set(final int index, final Boolean newElement) {
        return setIndexValue(index, newElement);
    }

    /**
     * Set the element at the specified index
     *
     * @param index the element index
     * @param newElement the new element
     * @return the previous element
     */
    public JsonElement set(final int index, final Number newElement) {
        return setIndexValue(index, newElement);
    }

    /**
     * Set the element at the specified index
     *
     * @param index the element index
     * @param newElement the new element
     * @return the previous element
     */
    public JsonElement set(final int index, final CharSequence newElement) {
        return setIndexValue(index, newElement);
    }

    /**
     * Set the element at the specified index
     *
     * @param index the element index
     * @param newElement the new element
     * @return the previous element
     */
    public JsonElement set(final int index, JsonElement newElement) {
        return setIndexValue(index, newElement);
    }

    /**
     * Get the index of an element in the
     * json array
     *
     * @param element the element
     * @return the index of the element
     */
    public int indexOf(final JsonElement element) {
        return this.elements.indexOf(element);
    }

    /**
     * Get the last index of an element
     * in the json array
     *
     * @param element the element
     * @return the last index of the element
     */
    public int lastIndexOf(final JsonElement element) {
        return this.elements.lastIndexOf(element);
    }

    /**
     * Get an element in the array
     *
     * @param index the element index
     * @return the element
     */
    public JsonElement get(final int index) {
        JsonPrimitive nul = validateIndex(index);
        if (nul != null) return nul;

        return this.elements.get(index);
    }

    /**
     * Get an element in the array
     *
     * @param index the element index
     * @param def the element default value
     * @return the element
     */
    public JsonElement getOrDefault(final int index, final JsonElement def) {
        JsonElement element = this.get(index);
        if (element == null || element.isNull())
            return def;

        return element;
    }

    /**
     * Remove an element from the
     * array
     *
     * @param bool the element to remove
     * @return if the element was removed
     */
    public boolean remove(final Boolean bool) {
        return this.elements.remove((bool == null ? JsonPrimitive.createNull() : new JsonPrimitive(bool)));
    }

    /**
     * Remove an element from the
     * array
     *
     * @param number the element to remove
     * @return if the element was removed
     */
    public boolean remove(final Number number) {
        return this.elements.remove((number == null ? JsonPrimitive.createNull() : new JsonPrimitive(number)));
    }

    /**
     * Remove an element from the
     * array
     *
     * @param sequence the element to remove
     * @return if the element was removed
     */
    public boolean remove(final CharSequence sequence) {
        return this.elements.remove((sequence == null ? JsonPrimitive.createNull() : new JsonPrimitive(sequence)));
    }

    /**
     * Remove an element from the
     * array
     *
     * @param element the element to remove
     * @return if the element was removed
     */
    public boolean remove(final JsonElement element) {
        return this.elements.remove(element);
    }

    /**
     * Remove an element from the array
     *
     * @param index the element index
     * @return the removed element
     */
    public JsonElement remove(final int index) {
        if (index < 0 || index >= this.elements.size())
            return null;

        return this.elements.remove(index);
    }

    /**
     * Remove any matching element from the
     * array
     *
     * @param bool the element to remove
     * @return if the element was removed
     */
    public boolean removeAny(final Boolean bool) {
        JsonElement element = (bool == null ? JsonPrimitive.createNull() : new JsonPrimitive(bool));
        return this.removeAny(element);
    }

    /**
     * Remove any matching element from the
     * array
     *
     * @param number the element to remove
     * @return if the element was removed
     */
    public boolean removeAny(final Number number) {
        JsonElement element = (number == null ? JsonPrimitive.createNull() : new JsonPrimitive(number));
        return this.removeAny(element);
    }

    /**
     * Remove any matching element from the
     * array
     *
     * @param sequence the element to remove
     * @return if the element was removed
     */
    public boolean removeAny(final CharSequence sequence) {
        JsonElement element = (sequence == null ? JsonPrimitive.createNull() : new JsonPrimitive(sequence));
        return this.removeAny(element);
    }

    /**
     * Remove any matching element from the
     * array
     *
     * @param element the element to remove
     * @return if the element was removed
     */
    public boolean removeAny(final JsonElement element) {
        return this.elements.removeIf((e) -> e.equals(element));
    }

    /**
     * Remove all the elements present in the
     * specified json array
     *
     * @param array the array to read elements
     *              from
     * @return if elements were removed
     */
    public boolean removeAll(final JsonArray array) {
        return this.elements.removeAll(array.elements);
    }

    /**
     * Remove all the elements present in the
     * specified collection
     *
     * @param elements the collection to read elements
     *              from
     * @return if elements were removed
     */
    public boolean removeAll(final Collection<? extends JsonElement> elements) {
        return this.elements.removeAll(elements);
    }

    /**
     * Get all the array elements
     *
     * @return the array elements
     */
    public Collection<JsonElement> getElements() {
        return Collections.unmodifiableCollection(this.elements);
    }

    private JsonElement setIndexValue(final int index, final Object value) {
        JsonPrimitive nul = validateIndex(index);
        if (nul != null) return nul;

        JsonElement element;
        if (value instanceof Boolean) {
            element = new JsonPrimitive((Boolean) value);
        } else if (value instanceof Number) {
            element = new JsonPrimitive((Number) value);
        } else if (value instanceof CharSequence) {
            element = new JsonPrimitive((CharSequence) value);
        } else if (value instanceof JsonElement) {
            element = (JsonElement) value;
        } else {
            element = JsonPrimitive.createNull();
        }

        element.setParent(this);

        return this.elements.set(index, element);
    }

    private JsonPrimitive validateIndex(final int index) {
        if (index < 0 || index >= this.elements.size()) {
            JsonPrimitive nul = JsonPrimitive.createNull();
            nul.setParent(this);

            return nul;
        }

        return null;
    }

    /**
     * Returns a hash code value for the object. This method is
     * supported for the benefit of hash tables such as those provided by
     * {@link HashMap}.
     * <p>
     * The general contract of {@code hashCode} is:
     * <ul>
     * <li>Whenever it is invoked on the same object more than once during
     *     an execution of a Java application, the {@code hashCode} method
     *     must consistently return the same integer, provided no information
     *     used in {@code equals} comparisons on the object is modified.
     *     This integer need not remain consistent from one execution of an
     *     application to another execution of the same application.
     * <li>If two objects are equal according to the {@code equals(Object)}
     *     method, then calling the {@code hashCode} method on each of
     *     the two objects must produce the same integer result.
     * <li>It is <em>not</em> required that if two objects are unequal
     *     according to the {@link Object#equals(Object)}
     *     method, then calling the {@code hashCode} method on each of the
     *     two objects must produce distinct integer results.  However, the
     *     programmer should be aware that producing distinct integer results
     *     for unequal objects may improve the performance of hash tables.
     * </ul>
     * <p>
     * As much as is reasonably practical, the hashCode method defined by
     * class {@code Object} does return distinct integers for distinct
     * objects. (This is typically implemented by converting the internal
     * address of the object into an integer, but this implementation
     * technique is not required by the
     * Java&trade; programming language.)
     *
     * @return a hash code value for this object.
     * @see Object#equals(Object)
     * @see System#identityHashCode
     */
    @Override
    public int hashCode() {
        return Objects.hashCode(this.elements);
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     * <p>
     * The {@code equals} method implements an equivalence relation
     * on non-null object references:
     * <ul>
     * <li>It is <i>reflexive</i>: for any non-null reference value
     *     {@code x}, {@code x.equals(x)} should return
     *     {@code true}.
     * <li>It is <i>symmetric</i>: for any non-null reference values
     *     {@code x} and {@code y}, {@code x.equals(y)}
     *     should return {@code true} if and only if
     *     {@code y.equals(x)} returns {@code true}.
     * <li>It is <i>transitive</i>: for any non-null reference values
     *     {@code x}, {@code y}, and {@code z}, if
     *     {@code x.equals(y)} returns {@code true} and
     *     {@code y.equals(z)} returns {@code true}, then
     *     {@code x.equals(z)} should return {@code true}.
     * <li>It is <i>consistent</i>: for any non-null reference values
     *     {@code x} and {@code y}, multiple invocations of
     *     {@code x.equals(y)} consistently return {@code true}
     *     or consistently return {@code false}, provided no
     *     information used in {@code equals} comparisons on the
     *     objects is modified.
     * <li>For any non-null reference value {@code x},
     *     {@code x.equals(null)} should return {@code false}.
     * </ul>
     * <p>
     * The {@code equals} method for class {@code Object} implements
     * the most discriminating possible equivalence relation on objects;
     * that is, for any non-null reference values {@code x} and
     * {@code y}, this method returns {@code true} if and only
     * if {@code x} and {@code y} refer to the same object
     * ({@code x == y} has the value {@code true}).
     * <p>
     * Note that it is generally necessary to override the {@code hashCode}
     * method whenever this method is overridden, so as to maintain the
     * general contract for the {@code hashCode} method, which states
     * that equal objects must have equal hash codes.
     *
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj
     * argument; {@code false} otherwise.
     * @see #hashCode()
     * @see HashMap
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) return true;

        if (!(obj instanceof JsonArray)) return false;
        JsonArray other = (JsonArray) obj;

        return other.elements.equals(this.elements);
    }

    /**
     * Returns a string representation of the object. In general, the
     * {@code toString} method returns a string that
     * "textually represents" this object. The result should
     * be a concise but informative representation that is easy for a
     * person to read.
     * It is recommended that all subclasses override this method.
     * <p>
     * The {@code toString} method for class {@code Object}
     * returns a string consisting of the name of the class of which the
     * object is an instance, the at-sign character `{@code @}', and
     * the unsigned hexadecimal representation of the hash code of the
     * object. In other words, this method returns a string equal to the
     * value of:
     * <blockquote>
     * <pre>
     * getClass().getName() + '@' + Integer.toHexString(hashCode())
     * </pre></blockquote>
     *
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return this.elements.toString();
    }
}
