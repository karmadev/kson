package com.github.karmadeb.kson.element;

import com.github.karmadeb.kson.exception.JsonCastException;
import com.github.karmadeb.kson.printer.JsonPrinter;
import com.github.karmadeb.kson.printer.type.JsonPrimitivePrinter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Objects;

/**
 * Represents a json primitive
 */
@SuppressWarnings("unused")
public final class JsonPrimitive extends JsonElement {

    private final JsonPrinter<JsonPrimitive> printer = new JsonPrimitivePrinter(this);
    private final Object value;

    /**
     * Create a null json primitive
     * object
     */
    private JsonPrimitive() {
        this.value = null;
    }

    /**
     * Create a primitive object from
     * the specified primitive value
     *
     * @param clone the clone to read from
     */
    public JsonPrimitive(final JsonPrimitive clone) {
        this.value = clone.value;
        this.setParent(clone.parent);
        this.setKey(clone.key);
    }

    /**
     * Create a primitive object for
     * the specified boolean value
     *
     * @param bool the value
     */
    public JsonPrimitive(final Boolean bool) {
        this.value = Objects.requireNonNull(bool);
    }

    /**
     * Create a primitive object for
     * the specified number value
     *
     * @param number the value
     */
    public JsonPrimitive(final Number number) {
        this.value = Objects.requireNonNull(number);
    }

    /**
     * Create a primitive object for
     * the specified char sequence
     *
     * @param sequence the value
     */
    public JsonPrimitive(final CharSequence sequence) {
        this.value = Objects.requireNonNull(sequence);
    }

    /**
     * Create a primitive object for
     * the specified character
     *
     * @param character the value
     */
    public JsonPrimitive(final Character character) {
        this(String.valueOf(Objects.requireNonNull(character)));
    }

    /**
     * Get if the element is a
     * boolean
     *
     * @return if the element is a
     * primitive type boolean
     */
    @Override
    public boolean isBoolean() {
        return this.value instanceof Boolean;
    }

    /**
     * Get the element as a boolean
     * value
     *
     * @return the element as a boolean
     */
    @Override
    public boolean getAsBoolean() {
        return (boolean) this.value;
    }

    /**
     * Get if the element is a
     * number
     *
     * @return if the element is a
     * primitive type number
     */
    @Override
    public boolean isNumber() {
        return this.value instanceof Number;
    }

    /**
     * Get the element as a number
     * value
     *
     * @return the element as a number
     */
    @Override
    public Number getAsNumber() {
        if (this.isNumber())
            return (Number) this.value;

        return tryParseNumber();
    }

    private Number tryParseNumber() {
        String raw = String.valueOf(this.value);
        if (raw.contains(".") || raw.contains("e")) {
            if (requiresBigDecimal(raw))
                return new BigDecimal(raw);

            try {
                return Float.parseFloat(raw);
            } catch (NumberFormatException ex) {
                return Double.parseDouble(raw);
            }
        }

        if (requiresBigInteger(raw))
            return new BigInteger(raw);

        return parseLittleNumber(raw);
    }

    private boolean requiresBigDecimal(final String str) {
        if (str.equalsIgnoreCase("infinity") || str.equalsIgnoreCase("-infinity") ||
                str.equalsIgnoreCase("nan")) return true;

        int comparisonToMax = str.compareTo(Double.toString(Double.MAX_VALUE));
        int comparisonToMin = str.compareTo(Double.toString(-Double.MAX_VALUE));

        return comparisonToMax > 0 || comparisonToMin < 0;
    }

    private boolean requiresBigInteger(final String str) {
        if (str.equalsIgnoreCase("infinity") || str.equalsIgnoreCase("-infinity") ||
                str.equalsIgnoreCase("nan")) return true;

        int comparisonToMax = str.compareTo(Long.toString(Long.MAX_VALUE));
        int comparisonToMin = str.compareTo(Long.toString(-Long.MAX_VALUE));

        return comparisonToMax > 0 || comparisonToMin < 0;
    }

    private Number parseLittleNumber(final String str) {
        try {
            return Byte.parseByte(str);
        } catch (NumberFormatException ignored) {}
        try {
            return Short.parseShort(str);
        } catch (NumberFormatException ignored) {}
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException ignored) {}
        try {
            return Long.parseLong(str);
        } catch (NumberFormatException ignored) {}

        throw new NumberFormatException("For input string: \"" + str + "\"");
    }

    /**
     * Get the element as a number
     * value
     *
     * @return the element as a number
     * @throws JsonCastException if the element is not primitive
     */
    @Override
    public byte getAsByte() throws JsonCastException {
        return this.value instanceof Number ? this.getAsNumber().byteValue() :
                Byte.parseByte(this.getAsString());
    }

    /**
     * Get the element as a number
     * value
     *
     * @return the element as a number
     * @throws JsonCastException if the element is not primitive
     */
    @Override
    public short getAsShort() throws JsonCastException {
        return this.value instanceof Number ? this.getAsNumber().shortValue() :
                Short.parseShort(this.getAsString());
    }

    /**
     * Get the element as a number
     * value
     *
     * @return the element as a number
     * @throws JsonCastException if the element is not primitive
     */
    @Override
    public int getAsInteger() throws JsonCastException {
        return this.value instanceof Number ? this.getAsNumber().intValue() :
                Integer.parseInt(this.getAsString());
    }

    /**
     * Get the element as a number
     * value
     *
     * @return the element as a number
     * @throws JsonCastException if the element is not primitive
     */
    @Override
    public long getAsLong() throws JsonCastException {
        return this.value instanceof Number ? this.getAsNumber().longValue() :
                Long.parseLong(this.getAsString());
    }

    /**
     * Get the element as a number
     * value
     *
     * @return the element as a number
     * @throws JsonCastException if the element is not primitive
     */
    @Override
    public float getAsFloat() throws JsonCastException {
        return this.value instanceof Number ? this.getAsNumber().floatValue() :
                Float.parseFloat(this.getAsString());
    }

    /**
     * Get the element as a number
     * value
     *
     * @return the element as a number
     * @throws JsonCastException if the element is not primitive
     */
    @Override
    public double getAsDouble() throws JsonCastException {
        return this.value instanceof Number ? this.getAsNumber().doubleValue() :
                Double.parseDouble(this.getAsString());
    }

    /**
     * Get the element as a number
     * value
     *
     * @return the element as a number
     * @throws JsonCastException if the element is not primitive
     */
    @Override
    public BigInteger getAsBigInteger() throws JsonCastException {
        if (this.value instanceof BigDecimal)
            return ((BigDecimal) this.value).toBigInteger();

        if (this.value instanceof BigInteger)
            return (BigInteger) this.value;

        return new BigInteger(this.getAsString());
    }

    /**
     * Get the element as a number
     * value
     *
     * @return the element as a number
     * @throws JsonCastException if the element is not primitive
     */
    @Override
    public BigDecimal getAsBigDecimal() throws JsonCastException {
        if (this.value instanceof BigDecimal)
            return (BigDecimal) this.value;

        if (this.value instanceof BigInteger)
            return new BigDecimal(this.value.toString());

        return new BigDecimal(this.getAsString());
    }

    /**
     * Get if the element is a
     * string
     *
     * @return if the element is a
     * primitive type string
     */
    @Override
    public boolean isString() {
        return this.value instanceof CharSequence;
    }

    /**
     * Get the element as a string
     * value
     *
     * @return the element as a string
     */
    @Override
    public String getAsString() {
        return String.valueOf(this.value);
    }

    /**
     * Get if the element is null
     *
     * @return if the element is null
     */
    @Override
    public boolean isNull() {
        return this.value == null;
    }

    /**
     * Get the element json printer
     *
     * @return the json printer of the
     * element
     */
    @Override
    public JsonPrinter<? extends JsonElement> getJsonPrinter() {
        return this.printer;
    }

    /**
     * Copy the element into a new
     * element
     */
    @Override
    public JsonPrimitive copy() {
        return new JsonPrimitive(this);
    }

    /**
     * Get the element size. The size
     * represents the amount of key -> values
     * this element has
     *
     * @return the element size
     */
    @Override
    public int size() {
        return 1;
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     * <p>
     * The {@code equals} method implements an equivalence relation
     * on non-null object references:
     * <ul>
     * <li>It is <i>reflexive</i>: for any non-null reference value
     *     {@code x}, {@code x.equals(x)} should return
     *     {@code true}.
     * <li>It is <i>symmetric</i>: for any non-null reference values
     *     {@code x} and {@code y}, {@code x.equals(y)}
     *     should return {@code true} if and only if
     *     {@code y.equals(x)} returns {@code true}.
     * <li>It is <i>transitive</i>: for any non-null reference values
     *     {@code x}, {@code y}, and {@code z}, if
     *     {@code x.equals(y)} returns {@code true} and
     *     {@code y.equals(z)} returns {@code true}, then
     *     {@code x.equals(z)} should return {@code true}.
     * <li>It is <i>consistent</i>: for any non-null reference values
     *     {@code x} and {@code y}, multiple invocations of
     *     {@code x.equals(y)} consistently return {@code true}
     *     or consistently return {@code false}, provided no
     *     information used in {@code equals} comparisons on the
     *     objects is modified.
     * <li>For any non-null reference value {@code x},
     *     {@code x.equals(null)} should return {@code false}.
     * </ul>
     * <p>
     * The {@code equals} method for class {@code Object} implements
     * the most discriminating possible equivalence relation on objects;
     * that is, for any non-null reference values {@code x} and
     * {@code y}, this method returns {@code true} if and only
     * if {@code x} and {@code y} refer to the same object
     * ({@code x == y} has the value {@code true}).
     * <p>
     * Note that it is generally necessary to override the {@code hashCode}
     * method whenever this method is overridden, so as to maintain the
     * general contract for the {@code hashCode} method, which states
     * that equal objects must have equal hash codes.
     *
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj
     * argument; {@code false} otherwise.
     * @see #hashCode()
     * @see HashMap
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) return true;
        if (obj == null)
            return this.value == null;

        if (!(obj instanceof JsonPrimitive)) return false;

        JsonPrimitive other = (JsonPrimitive) obj;
        return Objects.equals(other.value, this.value);
    }

    private boolean checkPrimitiveBoolean(final Object obj) {
        if (this.value instanceof Boolean)
            return obj.equals(this.value);

        return false;
    }

    private boolean checkPrimitiveNumber(final Object obj) {
        if (this.value instanceof Number)
            return obj.equals(this.value);

        return false;
    }

    private boolean checkPrimitiveCharSequence(final Object obj) {
        if (this.value instanceof CharSequence)
            return obj.equals(this.value);

        return false;
    }

    /**
     * Returns a string representation of the object. In general, the
     * {@code toString} method returns a string that
     * "textually represents" this object. The result should
     * be a concise but informative representation that is easy for a
     * person to read.
     * It is recommended that all subclasses override this method.
     * <p>
     * The {@code toString} method for class {@code Object}
     * returns a string consisting of the name of the class of which the
     * object is an instance, the at-sign character `{@code @}', and
     * the unsigned hexadecimal representation of the hash code of the
     * object. In other words, this method returns a string equal to the
     * value of:
     * <blockquote>
     * <pre>
     * getClass().getName() + '@' + Integer.toHexString(hashCode())
     * </pre></blockquote>
     *
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return String.valueOf(this.value);
    }

    /**
     * Returns a hash code value for the object. This method is
     * supported for the benefit of hash tables such as those provided by
     * {@link HashMap}.
     * <p>
     * The general contract of {@code hashCode} is:
     * <ul>
     * <li>Whenever it is invoked on the same object more than once during
     *     an execution of a Java application, the {@code hashCode} method
     *     must consistently return the same integer, provided no information
     *     used in {@code equals} comparisons on the object is modified.
     *     This integer need not remain consistent from one execution of an
     *     application to another execution of the same application.
     * <li>If two objects are equal according to the {@code equals(Object)}
     *     method, then calling the {@code hashCode} method on each of
     *     the two objects must produce the same integer result.
     * <li>It is <em>not</em> required that if two objects are unequal
     *     according to the {@link Object#equals(Object)}
     *     method, then calling the {@code hashCode} method on each of the
     *     two objects must produce distinct integer results.  However, the
     *     programmer should be aware that producing distinct integer results
     *     for unequal objects may improve the performance of hash tables.
     * </ul>
     * <p>
     * As much as is reasonably practical, the hashCode method defined by
     * class {@code Object} does return distinct integers for distinct
     * objects. (This is typically implemented by converting the internal
     * address of the object into an integer, but this implementation
     * technique is not required by the
     * Java&trade; programming language.)
     *
     * @return a hash code value for this object.
     * @see Object#equals(Object)
     * @see System#identityHashCode
     */
    @Override
    public int hashCode() {
        return Objects.hashCode(this.value);
    }

    /**
     * Creates a new json null
     * instance
     *
     * @return a json null
     */
    public static JsonPrimitive createNull() {
        return new JsonPrimitive();
    }
}