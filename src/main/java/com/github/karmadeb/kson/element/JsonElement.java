package com.github.karmadeb.kson.element;

import com.github.karmadeb.kson.exception.JsonCastException;
import com.github.karmadeb.kson.printer.JsonPrinter;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Objects;
import java.util.Optional;

/**
 * Represents a json element
 */
@SuppressWarnings("unused")
public abstract class JsonElement {

    private static final String EMPTY_STR = "";

    protected String key = EMPTY_STR;
    protected JsonElement parent;

    /**
     * Get the element key
     *
     * @return the element key in the
     * json tree
     */
    public final String getKey() {
        return this.key;
    }

    /**
     * Get the parent element of
     * this element
     *
     * @return the parent element
     */
    public final Optional<JsonElement> getParent() {
        return Optional.ofNullable(this.parent);
    }

    /**
     * Get the root element of this
     * element
     *
     * @return the root element
     */
    public final Optional<JsonElement> getRoot() {
        Optional<JsonElement> parent;
        do {
            parent = this.getParent();
        } while (parent.isPresent());

        return parent;
    }

    /**
     * Get if the element has a json-schema
     * attached to it
     *
     * @return if the element has a json-schema
     */
    public final boolean hasSchema() {
        if (!this.isObject()) return false;

        JsonObject object = this.getAsObject();
        if (!object.has("$schema")) return false;

        JsonElement schemaElement = object.get("$schema");
        if (!schemaElement.isPrimitive()) return false;

        JsonPrimitive primitive = schemaElement.getAsPrimitive();
        return primitive.isString();
    }

    /**
     * Get if the element has a parent
     * element
     *
     * @return if the element has a parent
     * element
     */
    public final boolean hasParent() {
        return this.getParent().isPresent();
    }

    /**
     * Get if this element is the
     * parent of the specified element
     *
     * @param child the child element
     * @return if this element is parent
     * of the element
     */
    public final boolean isParentOf(final JsonElement child) {
        if (child == null || this.equals(child)) return false;
        return Objects.equals(this, child.parent);
    }

    /**
     * Get if this element is the
     * child of the specified element
     *
     * @param parent the parent element
     * @return if this element is child
     * of the element
     */
    public final boolean isChildOf(final JsonElement parent) {
        if (parent == null || this.equals(parent)) return false;
        return Objects.equals(this.parent, parent);
    }

    /**
     * Get the element size. The size
     * represents the amount of key -> values
     * this element has
     *
     * @return the element size
     */
    public abstract int size();

    /**
     * Get if the element is empty
     *
     * @return if the element is
     * empty
     */
    public boolean isEmpty() {
        return this.size() == 0;
    }

    /**
     * Get if this element is a
     * json array
     *
     * @return if the element is
     * an array
     */
    public boolean isArray() {
        return this instanceof JsonArray;
    }

    /**
     * Get the element as a json
     * array
     *
     * @return the element json array
     */
    public JsonArray getAsArray() {
        return (JsonArray) this;
    }

    /**
     * Get if this element is a
     * json object
     *
     * @return if the element is
     * an object
     */
    public boolean isObject() {
        return this instanceof JsonObject;
    }

    /**
     * Get the element as a json
     * object
     *
     * @return the element json object
     */
    public JsonObject getAsObject() {
        return (JsonObject) this;
    }

    /**
     * Get if the element is a
     * json primitive
     *
     * @return if the element is
     * primitive
     */
    public boolean isPrimitive() {
        return this instanceof JsonPrimitive;
    }

    /**
     * Get the element as a json
     * primitive
     *
     * @return the element json primitive
     */
    public JsonPrimitive getAsPrimitive() {
        return (JsonPrimitive) this;
    }

    /**
     * Get if the element is a
     * boolean
     *
     * @return if the element is a
     * primitive type boolean
     */
    public boolean isBoolean() {
        if (this.isPrimitive()) {
            JsonPrimitive primitive = this.getAsPrimitive();
            return primitive.isBoolean();
        }

        return false;
    }

    /**
     * Get the element as a boolean
     * value
     *
     * @return the element as a boolean
     */
    public boolean getAsBoolean() {
        if (this.isPrimitive()) {
            JsonPrimitive primitive = this.getAsPrimitive();
            return primitive.getAsBoolean();
        }

        throw new JsonCastException(this.getClass(), JsonPrimitive.class);
    }

    /**
     * Get if the element is a
     * number
     *
     * @return if the element is a
     * primitive type number
     */
    public boolean isNumber() {
        if (this.isPrimitive()) {
            JsonPrimitive primitive = this.getAsPrimitive();
            return primitive.isNumber();
        }

        return false;
    }

    /**
     * Get the element as a number
     * value
     *
     * @return the element as a number
     * @throws JsonCastException if the element is not primitive
     */
    public Number getAsNumber() throws JsonCastException {
        if (this.isPrimitive()) {
            JsonPrimitive primitive = this.getAsPrimitive();
            return primitive.getAsNumber();
        }

        throw new JsonCastException(this.getClass(), JsonPrimitive.class);
    }

    /**
     * Get the element as a number
     * value
     *
     * @return the element as a number
     * @throws JsonCastException if the element is not primitive
     */
    public byte getAsByte() throws JsonCastException {
        return this.getAsNumber().byteValue();
    }

    /**
     * Get the element as a number
     * value
     *
     * @return the element as a number
     * @throws JsonCastException if the element is not primitive
     */
    public short getAsShort() throws JsonCastException {
        return this.getAsNumber().byteValue();
    }

    /**
     * Get the element as a number
     * value
     *
     * @return the element as a number
     * @throws JsonCastException if the element is not primitive
     */
    public int getAsInteger() throws JsonCastException {
        return this.getAsNumber().byteValue();
    }

    /**
     * Get the element as a number
     * value
     *
     * @return the element as a number
     * @throws JsonCastException if the element is not primitive
     */
    public long getAsLong() throws JsonCastException {
        return this.getAsNumber().byteValue();
    }

    /**
     * Get the element as a number
     * value
     *
     * @return the element as a number
     * @throws JsonCastException if the element is not primitive
     */
    public float getAsFloat() throws JsonCastException {
        return this.getAsNumber().byteValue();
    }

    /**
     * Get the element as a number
     * value
     *
     * @return the element as a number
     * @throws JsonCastException if the element is not primitive
     */
    public double getAsDouble() throws JsonCastException {
        return this.getAsNumber().byteValue();
    }

    /**
     * Get the element as a number
     * value
     *
     * @return the element as a number
     * @throws JsonCastException if the element is not primitive
     */
    public BigInteger getAsBigInteger() throws JsonCastException {
        return (BigInteger) this.getAsNumber();
    }

    /**
     * Get the element as a number
     * value
     *
     * @return the element as a number
     * @throws JsonCastException if the element is not primitive
     */
    public BigDecimal getAsBigDecimal() throws JsonCastException {
        return (BigDecimal) this.getAsNumber();
    }

    /**
     * Get if the element is a
     * string
     *
     * @return if the element is a
     * primitive type string
     */
    public boolean isString() {
        if (this.isPrimitive()) {
            JsonPrimitive primitive = this.getAsPrimitive();
            return primitive.isString();
        }

        return false;
    }

    /**
     * Get the element as a string
     * value
     *
     * @return the element as a string
     */
    public String getAsString() {
        if (this.isPrimitive()) {
            JsonPrimitive primitive = this.getAsPrimitive();
            return primitive.getAsString();
        }

        throw new JsonCastException(this.getClass(), JsonPrimitive.class);
    }

    /**
     * Get if the element is null
     *
     * @return if the element is null
     */
    public boolean isNull() {
        if (this.isPrimitive()) {
            JsonPrimitive primitive = this.getAsPrimitive();
            return primitive.isNull();
        }

        return false;
    }

    /**
     * Get if this element is a
     * json array
     *
     * @param key the element key
     * @return if the element is
     * an array
     * @throws JsonCastException if the current element
     * is not a json object
     */
    public boolean isArray(final String key) throws JsonCastException {
        if (this.isObject()) {
            return this.getAsObject()
                    .get(key).isArray();
        }

        throw new JsonCastException(this.getClass(), JsonObject.class);
    }

    /**
     * Get the element as a json
     * array
     *
     * @param key the element key
     * @return the element json array
     * @throws JsonCastException if the current element
     * is not a json object
     */
    public JsonArray getAsArray(final String key) throws JsonCastException {
        if (this.isObject()) {
            return this.getAsObject()
                    .get(key).getAsArray();
        }

        throw new JsonCastException(this.getClass(), JsonObject.class);
    }

    /**
     * Get if this element is a
     * json object
     *
     * @param key the element key
     * @return if the element is
     * an object
     * @throws JsonCastException if the current element
     * is not a json object
     */
    public boolean isObject(final String key) throws JsonCastException {
        if (this.isObject()) {
            return this.getAsObject()
                    .get(key).isObject();
        }

        throw new JsonCastException(this.getClass(), JsonObject.class);
    }

    /**
     * Get the element as a json
     * object
     *
     * @param key the element key
     * @return the element json object
     * @throws JsonCastException if the current element
     * is not a json object
     */
    public JsonObject getAsObject(final String key) throws JsonCastException {
        if (this.isObject()) {
            return this.getAsObject()
                    .get(key).getAsObject();
        }

        throw new JsonCastException(this.getClass(), JsonObject.class);
    }

    /**
     * Get if the element is a
     * json primitive
     *
     * @param key the element key
     * @return if the element is
     * primitive
     * @throws JsonCastException if the current element
     * is not a json object
     */
    public boolean isPrimitive(final String key) throws JsonCastException {
        if (this.isObject()) {
            return this.getAsObject()
                    .get(key).isPrimitive();
        }

        throw new JsonCastException(this.getClass(), JsonObject.class);
    }

    /**
     * Get the element as a json
     * primitive
     *
     * @param key the element key
     * @return the element json primitive
     * @throws JsonCastException if the current element
     * is not a json object
     */
    public JsonPrimitive getAsPrimitive(final String key) throws JsonCastException {
        if (this.isObject()) {
            return this.getAsObject()
                    .get(key).getAsPrimitive();
        }

        throw new JsonCastException(this.getClass(), JsonObject.class);
    }

    /**
     * Get if the element is a
     * boolean
     *
     * @param key the element key
     * @return if the element is a
     * primitive type boolean
     * @throws JsonCastException if the current element
     * is not a json object
     */
    public boolean isBoolean(final String key) throws JsonCastException {
        if (this.isObject()) {
            return this.getAsObject()
                    .get(key).isBoolean();
        }

        throw new JsonCastException(this.getClass(), JsonObject.class);
    }

    /**
     * Get the element as a boolean
     * value
     *
     * @param key the element key
     * @return the element as a boolean
     * @throws JsonCastException if the current element
     * is not a json object
     */
    public boolean getAsBoolean(final String key) throws JsonCastException {
        if (this.isObject()) {
            return this.getAsObject()
                    .get(key).getAsBoolean();
        }

        throw new JsonCastException(this.getClass(), JsonObject.class);
    }

    /**
     * Get if the element is a
     * number
     *
     * @param key the element key
     * @return if the element is a
     * primitive type number
     * @throws JsonCastException if the current element
     * is not a json object
     */
    public boolean isNumber(final String key) throws JsonCastException {
        if (this.isObject()) {
            return this.getAsObject()
                    .get(key).isNumber();
        }

        throw new JsonCastException(this.getClass(), JsonObject.class);
    }

    /**
     * Get the element as a number
     * value
     *
     * @param key the element key
     * @return the element as a number
     * @throws JsonCastException if the element is not primitive or
     * the current element is not an object
     */
    public Number getAsNumber(final String key) throws JsonCastException {
        if (this.isObject()) {
            return this.getAsObject()
                    .get(key).getAsNumber();
        }

        throw new JsonCastException(this.getClass(), JsonObject.class);
    }

    /**
     * Get the element as a number
     * value
     *
     * @param key the element key
     * @return the element as a number
     * @throws JsonCastException if the element is not primitive or
     * the current element is not an object
     */
    public byte getAsByte(final String key) throws JsonCastException {
        if (this.isObject()) {
            return this.getAsObject()
                    .get(key).getAsByte();
        }

        throw new JsonCastException(this.getClass(), JsonObject.class);
    }

    /**
     * Get the element as a number
     * value
     *
     * @param key the element key
     * @return the element as a number
     * @throws JsonCastException if the element is not primitive or
     * the current element is not an object
     */
    public short getAsShort(final String key) throws JsonCastException {
        if (this.isObject()) {
            return this.getAsObject()
                    .get(key).getAsShort();
        }

        throw new JsonCastException(this.getClass(), JsonObject.class);
    }

    /**
     * Get the element as a number
     * value
     *
     * @param key the element key
     * @return the element as a number
     * @throws JsonCastException if the element is not primitive or
     * the current element is not an object
     */
    public int getAsInteger(final String key) throws JsonCastException {
        if (this.isObject()) {
            return this.getAsObject()
                    .get(key).getAsInteger();
        }

        throw new JsonCastException(this.getClass(), JsonObject.class);
    }

    /**
     * Get the element as a number
     * value
     *
     * @param key the element key
     * @return the element as a number
     * @throws JsonCastException if the element is not primitive or
     * the current element is not an object
     */
    public long getAsLong(final String key) throws JsonCastException {
        if (this.isObject()) {
            return this.getAsObject()
                    .get(key).getAsLong();
        }

        throw new JsonCastException(this.getClass(), JsonObject.class);
    }

    /**
     * Get the element as a number
     * value
     *
     * @param key the element key
     * @return the element as a number
     * @throws JsonCastException if the element is not primitive or
     * the current element is not an object
     */
    public float getAsFloat(final String key) throws JsonCastException {
        if (this.isObject()) {
            return this.getAsObject()
                    .get(key).getAsFloat();
        }

        throw new JsonCastException(this.getClass(), JsonObject.class);
    }

    /**
     * Get the element as a number
     * value
     *
     * @param key the element key
     * @return the element as a number
     * @throws JsonCastException if the element is not primitive or
     * the current element is not an object
     */
    public double getAsDouble(final String key) throws JsonCastException {
        if (this.isObject()) {
            return this.getAsObject()
                    .get(key).getAsDouble();
        }

        throw new JsonCastException(this.getClass(), JsonObject.class);
    }

    /**
     * Get the element as a number
     * value
     *
     * @param key the element key
     * @return the element as a number
     * @throws JsonCastException if the element is not primitive or
     * the current element is not an object
     */
    public BigInteger getAsBigInteger(final String key) throws JsonCastException {
        if (this.isObject()) {
            return this.getAsObject()
                    .get(key).getAsBigInteger();
        }

        throw new JsonCastException(this.getClass(), JsonObject.class);
    }

    /**
     * Get the element as a number
     * value
     *
     * @param key the element key
     * @return the element as a number
     * @throws JsonCastException if the element is not primitive or
     * the current element is not an object
     */
    public BigDecimal getAsBigDecimal(final String key) throws JsonCastException {
        if (this.isObject()) {
            return this.getAsObject()
                    .get(key).getAsBigDecimal();
        }

        throw new JsonCastException(this.getClass(), JsonObject.class);
    }

    /**
     * Get if the element is a
     * string
     *
     * @param key the element key
     * @return if the element is a
     * primitive type string
     * @throws JsonCastException if the current element
     * is not a json object
     */
    public boolean isString(final String key) throws JsonCastException {
        if (this.isObject()) {
            return this.getAsObject()
                    .get(key).isString();
        }

        throw new JsonCastException(this.getClass(), JsonObject.class);
    }

    /**
     * Get the element as a string
     * value
     *
     * @param key the element key
     * @return the element as a string
     * @throws JsonCastException if the current element
     * is not a json object
     */
    public String getAsString(final String key) throws JsonCastException {
        if (this.isObject()) {
            return this.getAsObject()
                    .get(key).getAsString();
        }

        throw new JsonCastException(this.getClass(), JsonObject.class);
    }

    /**
     * Get if the specified element is null
     *
     * @param key the element key
     * @return if the element is null
     * @throws JsonCastException if the current element
     * is not a json object
     */
    public boolean isNull(final String key) throws JsonCastException {
        if (this.isObject()) {
            return this.getAsObject()
                    .get(key).isNumber();
        }

        throw new JsonCastException(this.getClass(), JsonObject.class);
    }

    /**
     * Get the element json printer
     *
     * @return the json printer of the
     * element
     */
    public abstract JsonPrinter<? extends JsonElement> getJsonPrinter();

    /**
     * Get the json string representation of
     * this element
     *
     * @return the element as a string
     */
    public String toJsonString() {
        return this.toJsonString(0);
    }

    /**
     * Get the json string representation of
     * this element
     *
     * @param indentation the string ident
     * @return the element as a string
     */
    public String toJsonString(final int indentation) {
        return this.toJsonString(indentation, ' ');
    }

    /**
     * Get the json string representation of
     * this element
     *
     * @param indentation the string ident
     * @param indentationCharacter the indentation character
     * @return the element as a string
     */
    public String toJsonString(final int indentation, final char indentationCharacter) {
        StringWriter writer = new StringWriter();

        JsonPrinter<?> printer = this.getJsonPrinter();
        printer.reset(); //Just in case

        printer.withIndent(indentation)
                .withIndentChar(indentationCharacter)
                .print(1, writer);

        String result = writer.toString();
        printer.reset();

        return result;
    }

    /**
     * Copy the element into a new
     * element
     */
    public abstract JsonElement copy();

    /**
     * Set the element key
     *
     * @param key the key
     */
    final void setKey(final String key) {
        this.key = (key == null ? EMPTY_STR : key);
    }

    /**
     * Set the element parent
     *
     * @param parent the parent element
     */
    final void setParent(final JsonElement parent) {
        this.parent = parent;
    }
}