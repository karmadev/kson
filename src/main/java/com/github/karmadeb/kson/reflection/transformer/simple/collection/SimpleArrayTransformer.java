package com.github.karmadeb.kson.reflection.transformer.simple.collection;

import com.github.karmadeb.kson.element.JsonArray;
import com.github.karmadeb.kson.element.JsonElement;
import com.github.karmadeb.kson.reflection.ObjectTransformer;
import com.github.karmadeb.kson.reflection.transformer.TransformerUtils;

/**
 * Represents an array transformer
 */
public final class SimpleArrayTransformer implements ObjectTransformer<Object[]> {

    /**
     * Transform the element into the
     * json element
     *
     * @param element the element
     * @return the json element
     */
    @Override
    public JsonArray transform(final Object[] element) {
        JsonArray array = new JsonArray();

        for (Object content : element) {
            array.add(TransformerUtils.simpleTransform(content));
        }

        return array;
    }

    /**
     * Transform from the json element
     * into the element
     *
     * @param element the json element
     * @return the element
     */
    @Override
    public Object[] resolve(final JsonElement element) {
        throw new UnsupportedOperationException("Cannot resolve from simple transformer");
    }
}
