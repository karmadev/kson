package com.github.karmadeb.kson.reflection.transformer.collection.map;

import com.github.karmadeb.kson.element.JsonElement;
import com.github.karmadeb.kson.element.JsonObject;
import com.github.karmadeb.kson.exception.reflection.JsonSerializationException;
import com.github.karmadeb.kson.reflection.ObjectTransformer;
import com.github.karmadeb.kson.reflection.transformer.TransformerUtils;

import java.util.AbstractMap;
import java.util.Map;

/**
 * Represents a map entry (key-value object) transformer
 */
public class MapEntryTransformer implements ObjectTransformer<Map.Entry<?, ?>> {

    /**
     * Transform the element into the
     * json element
     *
     * @param entry the element
     * @return the json element
     */
    @Override
    public JsonObject transform(final Map.Entry<?, ?> entry) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.set("$transformer", MapEntryTransformer.class.getCanonicalName());

        JsonObject valueData = new JsonObject();
        Object key = entry.getKey();
        Object value = entry.getValue();

        JsonElement k = TransformerUtils.transform(key);
        JsonElement v = TransformerUtils.transform(value);
        valueData.set("key", k);
        valueData.set("value", v);

        jsonObject.set("$value", valueData);
        return jsonObject;
    }

    /**
     * Transform from the json element
     * into the element
     *
     * @param element the json element
     * @return the element
     */
    @Override
    public Map.Entry<?, ?> resolve(final JsonElement element) {
        if (!element.isObject())
            throw new JsonSerializationException("Failed to resolve " + element.toJsonString() + " to Map.Entry");

        JsonObject jsonObject = element.getAsObject();
        if (!jsonObject.has("$value"))
            throw new JsonSerializationException("Failed to resolve " + element.toJsonString() + " to Map.Entry");

        JsonElement serializedElement = jsonObject.get("$value");
        if (!serializedElement.isObject())
            throw new JsonSerializationException("Failed to resolve " + element.toJsonString() + " to Map.Entry");

        jsonObject = serializedElement.getAsObject();
        if (!jsonObject.has("key") || !jsonObject.has("value"))
            throw new JsonSerializationException("Failed to resolve " + element.toJsonString() + " to Map.Entry");

        JsonElement keyElement = jsonObject.get("key");
        JsonElement valueElement = jsonObject.get("value");

        Object key = TransformerUtils.resolve(keyElement);
        Object value = TransformerUtils.resolve(valueElement);

        return new AbstractMap.SimpleEntry<>(key, value);
    }
}
