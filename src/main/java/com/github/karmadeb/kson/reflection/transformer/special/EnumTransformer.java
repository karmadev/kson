package com.github.karmadeb.kson.reflection.transformer.special;

import com.github.karmadeb.kson.element.JsonElement;
import com.github.karmadeb.kson.element.JsonObject;
import com.github.karmadeb.kson.exception.reflection.JsonSerializationException;
import com.github.karmadeb.kson.reflection.ObjectTransformer;

/**
 * Represents an enum transformer
 */
public final class EnumTransformer implements ObjectTransformer<Enum<?>> {

    /**
     * Transform the element into the
     * json element
     *
     * @param element the element
     * @return the json element
     */
    @Override
    public JsonElement transform(final Enum<?> element) {
        JsonObject object = new JsonObject();
        object.set("$transformer", EnumTransformer.class.getCanonicalName());
        object.set("$class", element.getDeclaringClass().getCanonicalName());
        object.set("$serial", Long.MIN_VALUE);
        object.set("$value", element.name());

        return object;
    }

    /**
     * Transform from the json element
     * into the element
     *
     * @param element the json element
     * @return the element
     */
    @Override
    public Enum<?> resolve(final JsonElement element) {
        if (!element.isObject())
            return null;

        JsonObject object = element.getAsObject();
        if (object.has("$class") && object.has("$serial") && object.has("$value")) {
            JsonElement type = object.get("$class");
            JsonElement serial = object.get("$serial");
            JsonElement value = object.get("$value");

            if (!type.isString() || !serial.isNumber() || !type.isString())
                return null;

            String rawType = type.getAsString();
            long serialId = serial.getAsLong();
            String rawValue = value.getAsString();

            if (serialId != Long.MIN_VALUE)
                throw new JsonSerializationException("Failed to deserialize " + rawType + " as enum. Invalid or corrupt serialization data");

            return resolveEnumConstant(rawType, rawValue);
        }

        throw new JsonSerializationException("Failed to deserialize object " + element.toJsonString() + " as enum");
    }

    @SuppressWarnings("unchecked")
    private static Enum<?> resolveEnumConstant(String rawType, String rawValue) {
        try {
            Class<? extends Enum<?>> enumClass = (Class<? extends Enum<?>>) Class.forName(rawType).asSubclass(Enum.class);
            Enum<?>[] constants = enumClass.getEnumConstants();

            for (Enum<?> constant : constants) {
                if (constant.name().equalsIgnoreCase(rawValue))
                    return constant;
            }

            throw new EnumConstantNotPresentException(enumClass, rawValue);
        } catch (ClassNotFoundException ex) {
            throw new JsonSerializationException("Failed to deserialize enum " + rawType);
        }
    }
}
