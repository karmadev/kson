package com.github.karmadeb.kson.reflection.transformer.collection.map;

import com.github.karmadeb.kson.element.JsonArray;
import com.github.karmadeb.kson.element.JsonElement;
import com.github.karmadeb.kson.element.JsonObject;
import com.github.karmadeb.kson.exception.reflection.JsonSerializationException;
import com.github.karmadeb.kson.reflection.ObjectTransformer;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Represents a map (key = value object) transformer
 */
public final class MapTransformer implements ObjectTransformer<Map<?, ?>> {

    /**
     * Transform the element into the
     * json element
     *
     * @param kvMap the element
     * @return the json element
     */
    @Override
    public JsonObject transform(final Map<?, ?> kvMap) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.set("$transformer", MapTransformer.class.getName());

        JsonArray jsonArray = new JsonArray();
        for (Map.Entry<?, ?> entry : kvMap.entrySet()) {
            MapEntryTransformer entryTransformer = new MapEntryTransformer();
            JsonObject entryJsonObject = entryTransformer.transform(entry);

            jsonArray.add(entryJsonObject);
        }

        jsonObject.set("$value", jsonArray);
        return jsonObject;
    }

    /**
     * Transform from the json element
     * into the element
     *
     * @param element the json element
     * @return the element
     */
    @Override
    public Map<?, ?> resolve(final JsonElement element) {
        Map<Object, Object> map = new LinkedHashMap<>();
        if (!element.isArray())
            throw new JsonSerializationException("Failed to resolve " + element.toJsonString() + " to Map");

        MapEntryTransformer entryTransformer = new MapEntryTransformer();
        for (JsonElement arrayElement : element.getAsArray()) {
            if (!arrayElement.isObject()) continue;

            Map.Entry<?, ?> entry = entryTransformer.resolve(arrayElement);
            map.put(entry.getKey(), entry.getValue());
        }

        return map;
    }
}
