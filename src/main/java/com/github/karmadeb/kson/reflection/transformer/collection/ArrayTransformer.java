package com.github.karmadeb.kson.reflection.transformer.collection;

import com.github.karmadeb.kson.element.JsonArray;
import com.github.karmadeb.kson.element.JsonElement;
import com.github.karmadeb.kson.element.JsonObject;
import com.github.karmadeb.kson.reflection.ObjectTransformer;
import com.github.karmadeb.kson.reflection.transformer.TransformerUtils;

/**
 * Represents an array transformer
 */
public final class ArrayTransformer implements ObjectTransformer<Object[]> {

    /**
     * Transform the element into the
     * json element
     *
     * @param element the element
     * @return the json element
     */
    @Override
    public JsonElement transform(final Object[] element) {
        JsonObject object = new JsonObject();
        object.set("$transformer", ArrayTransformer.class.getCanonicalName());

        JsonArray array = new JsonArray();

        for (int i = 0; i < element.length; i++) {
            Object content = element[i];
            JsonObject elementObject = new JsonObject();
            elementObject.set("$index", i);
            elementObject.set("$value", TransformerUtils.transform(content));

            array.add(elementObject);
        }

        object.set("$value", array);
        return object;
    }

    /**
     * Transform from the json element
     * into the element
     *
     * @param element the json element
     * @return the element
     */
    @Override
    public Object[] resolve(final JsonElement element) {
        if (!element.isArray())
            return new Object[0];

        JsonArray jsonArray = element.getAsArray();

        Object[] values = new Object[jsonArray.size()];
        for (int i = 0; i < values.length; i++) {
            JsonElement arrayElement = jsonArray.get(i);
            if (!arrayElement.isObject()) continue;

            JsonObject elementObject = arrayElement.getAsObject();
            if (!elementObject.has("$index") || !elementObject.has("$value")) continue;

            JsonElement indexElement = elementObject.get("$index");
            JsonElement valueElement = elementObject.get("$value");
            if (!indexElement.isNumber() || !valueElement.isObject()) continue;

            int index = indexElement.getAsInteger();
            JsonObject value = valueElement.getAsObject();

            Object transformed = TransformerUtils.resolve(value);
            values[index] = transformed;
        }

        return values;
    }
}
