package com.github.karmadeb.kson.reflection;

import com.github.karmadeb.kson.annotation.serialization.JsonInclude;
import com.github.karmadeb.kson.element.JsonElement;
import com.github.karmadeb.kson.exception.reflection.JsonSerializationException;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a json serializer
 *
 * @param <T> the json output type
 */
public abstract class JsonSerializer<T extends JsonElement> {

    protected final Object instance;

    /**
     * Create a new json serializer
     *
     * @param instance the object instance to serialize
     */
    public JsonSerializer(final Object instance) {
        this.instance = instance;
    }

    /**
     * Set if the serializer works on
     * simple mode
     *
     * @param simpleMode the simple mode status
     * @throws UnsupportedOperationException if the serializer does not
     * support this operation
     */
    public abstract void setSimple(final boolean simpleMode) throws UnsupportedOperationException;

    /**
     * Process the object
     *
     * @throws JsonSerializationException if the object fails to process
     */
    public abstract void process() throws JsonSerializationException;

    /**
     * Serialize the instance
     *
     * @return the serialized instance
     */
    public abstract T serialize();

    protected static List<Field> getIncludeFields(final Object instance) {
        List<Field> included = new ArrayList<>();

        Field[] fields = instance.getClass().getDeclaredFields();
        for (Field field : fields) {
            int modifiers = field.getModifiers();
            if (Modifier.isStatic(modifiers)) continue;

            if (Modifier.isPublic(modifiers)) {
                included.add(field);
                continue;
            }

            if (field.isAnnotationPresent(JsonInclude.class))
                included.add(field);
        }

        return included;
    }
}