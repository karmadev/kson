package com.github.karmadeb.kson.reflection.transformer.primitive;

import com.github.karmadeb.kson.element.JsonElement;
import com.github.karmadeb.kson.element.JsonObject;
import com.github.karmadeb.kson.reflection.ObjectTransformer;

/**
 * Represents a boolean transformer
 */
public final class BooleanTransformer implements ObjectTransformer<Boolean> {

    /**
     * Transform the element into the
     * json element
     *
     * @param element the element
     * @return the json element
     */
    @Override
    public JsonElement transform(final Boolean element) {
        JsonObject object = new JsonObject();
        object.set("$transformer", BooleanTransformer.class.getCanonicalName());
        object.set("$value", element);

        return object;
    }

    /**
     * Transform from the json element
     * into the element
     *
     * @param element the json element
     * @return the element
     */
    @Override
    public Boolean resolve(final JsonElement element) {
        if (!element.isBoolean())
            return false;

        return element.getAsBoolean();
    }
}
