package com.github.karmadeb.kson.reflection.serialization;

import com.github.karmadeb.kson.annotation.serialization.*;
import com.github.karmadeb.kson.annotation.serialization.filter.JsonFieldInclude;
import com.github.karmadeb.kson.annotation.serialization.filter.JsonNullInclude;
import com.github.karmadeb.kson.element.JsonObject;
import com.github.karmadeb.kson.exception.reflection.JsonSerializationException;
import com.github.karmadeb.kson.reflection.JsonSerializer;
import com.github.karmadeb.kson.reflection.ObjectTransformer;
import com.github.karmadeb.kson.reflection.field.TransformableField;
import com.github.karmadeb.kson.reflection.transformer.TransformerUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;

/**
 * Represents a serializer for objects
 * which are annotated with {@link JsonSerializable}
 */
public final class JsonClassSerializer extends JsonSerializer<JsonObject> {

    private static final Object NULL_POINTER = new Object();

    private boolean simple = false;
    private boolean requiresSimple = false;
    private final List<TransformableField> parsedFields = new ArrayList<>();

    private long serial = Long.MIN_VALUE;

    /**
     * Create a new json serializer
     *
     * @param instance the object instance to serialize
     */
    public JsonClassSerializer(final Object instance) {
        super(instance);
    }

    /**
     * Set if the serializer works on
     * simple mode
     *
     * @param simpleMode the simple mode status
     * @throws UnsupportedOperationException if the serializer does not
     *                                       support this operation
     */
    @Override
    public void setSimple(final boolean simpleMode) throws UnsupportedOperationException {
        this.simple = simpleMode;
    }

    /**
     * Process the object
     *
     * @throws JsonSerializationException if the object fails to process
     */
    @Override @SuppressWarnings("unchecked")
    public void process() throws JsonSerializationException {
        List<Field> fields = getIncludeFields(this.instance);

        Class<?> instanceType = this.instance.getClass();
        if (!instanceType.isAnnotationPresent(JsonSerializable.class))
            throw new JsonSerializationException("Failed to serialize " + this.instance.getClass().getSimpleName() + " because it's not serializable");

        JsonSerializable classMeta = instanceType.getAnnotation(JsonSerializable.class);
        if (classMeta.simpleOutput())
            this.requiresSimple = true;

        this.serial = Math.abs(classMeta.serialId());

        for (Field field : fields) {
            Object specialInstance = this.instance;
            String name = null;
            if (fieldDoesNotPassThrough(classMeta, field))
                continue;
            if (validateNullability(classMeta, field, this.instance))
                continue;

            if (field.isAnnotationPresent(JsonFieldInclude.class)) {
                JsonFieldInclude include = field.getAnnotation(JsonFieldInclude.class);

                name = field.getName();
                if (field.isAnnotationPresent(JsonInclude.class)) {
                    JsonInclude i = field.getAnnotation(JsonInclude.class);
                    name = i.name();
                }
                if (name != null && name.equalsIgnoreCase("%default%"))
                    name = field.getName();

                VirtualField virtualField = resolveFieldInclude(field, instance, name, include);
                if (virtualField == null)
                    throw new JsonSerializationException("Failed to resolve field-only include on " + field.getName() + " from " + instanceType.getCanonicalName());

                field = virtualField.field;
                specialInstance = virtualField.instance;
            }

            TransformableField resolvedField;
            try {
                if (field.isAnnotationPresent(SpecialTransformer.class)) {
                    SpecialTransformer special = field.getAnnotation(SpecialTransformer.class);

                    Class<? extends ObjectTransformer<?>> transformerClass = special.transformer();

                    Constructor<? extends ObjectTransformer<?>> constructor = transformerClass.getDeclaredConstructor();
                    ObjectTransformer<Object> transformer = (ObjectTransformer<Object>) constructor.newInstance();

                    resolvedField = new TransformableField(specialInstance, field, name, transformer);
                } else {
                    resolvedField = resolveTransformableForField(field, name, specialInstance);
                }

                this.parsedFields.add(resolvedField);
            } catch (ReflectiveOperationException ex) {
                throw new JsonSerializationException("Failed to resolve field", ex);
            }
        }
    }

    private TransformableField resolveTransformableForField(final Field field, final String name, final Object instance) throws ReflectiveOperationException {
        Class<?> fieldType = field.getType();
        ObjectTransformer<Object> transformer = ((this.simple || this.requiresSimple) ? TransformerUtils.getSimpleTransformerFromType(fieldType) :
                TransformerUtils.getTransformerFromType(fieldType));
        return new TransformableField(instance, field, name, transformer);
    }

    /**
     * Serialize the instance
     *
     * @return the serialized instance
     */
    @Override
    public JsonObject serialize() {
        if (this.serial == Long.MIN_VALUE)
            throw new JsonSerializationException("Cannot serialize " + this.instance.getClass().getSimpleName() + " because it does not have serializable fields");

        JsonObject object = new JsonObject();
        if (!this.requiresSimple && !this.simple) {
            object.set("$class", this.instance.getClass().getCanonicalName());
            object.set("$serial", this.serial);
        }

        for (TransformableField field : this.parsedFields)
            object.set(field.getName(), field.getTransformed());

        return object;
    }

    private VirtualField resolveFieldInclude(final Field field, final Object instance, final String name, final JsonFieldInclude include) {
        String fName = include.field();

        Object value = tryGetObject(field, instance).orElse(null);
        if (value == null) return null;
        if (Objects.equals(value, NULL_POINTER))
            return new VirtualField(field, instance, name);

        Class<?> valueType = value.getClass();
        if (!valueType.isAnnotationPresent(JsonSerializable.class))
            throw new JsonSerializationException("Cannot read from class " + valueType.getCanonicalName() + " requested by " + include.getClass().getCanonicalName() + "#" + field.getName() + " because it's not serializable");

        try {
            Field valueField = valueType.getDeclaredField(fName);

            JsonSerializable serializable = valueType.getAnnotation(JsonSerializable.class);
            if (fieldDoesNotPassThrough(serializable, valueField))
                throw new JsonSerializationException("Cannot read field " + fName + " from " + field.getType().getCanonicalName() + " from " + instance.getClass().getCanonicalName() + "#" + field.getName() + " because parent class JsonSerializer prevents it");

            return new VirtualField(valueField, value, name);
        } catch (ReflectiveOperationException ex) {
            throw new JsonSerializationException("Failed to read field " + fName + " from " + field.getType().getCanonicalName() + " from " + instance.getClass().getCanonicalName() + "#" + field.getName(), ex);
        }
    }

    private boolean fieldDoesNotPassThrough(final JsonSerializable annotation, final Field field) {
        boolean publicFieldsAuto = annotation.publicFields();
        boolean onlyIncluded = annotation.explicitlyIncludedOnly();

        if (publicFieldsAuto && onlyIncluded)
            onlyIncluded = false;

        if (field.isAnnotationPresent(JsonExclude.class))
            return true;

        int modifiers = field.getModifiers();
        if (field.isAnnotationPresent(JsonInclude.class) || (Modifier.isPublic(modifiers) && publicFieldsAuto))
            return false;

        if (onlyIncluded)
            return field.isAnnotationPresent(JsonInclude.class);

        return publicFieldsAuto;
    }

    private boolean validateNullability(final JsonSerializable serializable, final Field field, final Object instance) {
        boolean ignoreNull = !serializable.serializeNulls();
        if (field.isAnnotationPresent(JsonNullInclude.class)) {
            JsonNullInclude include = field.getAnnotation(JsonNullInclude.class);
            ignoreNull = !include.include();
        }

        if (ignoreNull) {
            Optional<Object> value = tryGetObject(field, instance);
            if (value.isPresent()) {
                Object obj = value.get();
                return Objects.equals(obj, NULL_POINTER);
            }
        }

        return false;
    }

    private Optional<Object> tryGetObject(final Field field, final Object instance) {
        try {
            field.setAccessible(true);
            Object obj = field.get(instance);
            if (obj == null)
                obj = NULL_POINTER;

            return Optional.of(obj);
        } catch (ReflectiveOperationException ignored) {}

        return Optional.empty();
    }

    private static class VirtualField {

        public final Field field;
        public final Object instance;
        public final String name;

        public VirtualField(final Field field, final Object instance, final String name) {
            this.field = field;
            this.instance = instance;
            this.name = name;
        }
    }
}