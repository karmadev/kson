package com.github.karmadeb.kson.reflection.transformer.simple.special;

import com.github.karmadeb.kson.element.JsonElement;
import com.github.karmadeb.kson.element.JsonPrimitive;
import com.github.karmadeb.kson.reflection.ObjectTransformer;

/**
 * Represents an enum transformer
 */
public final class SimpleEnumTransformer implements ObjectTransformer<Enum<?>> {

    /**
     * Transform the element into the
     * json element
     *
     * @param element the element
     * @return the json element
     */
    @Override
    public JsonElement transform(final Enum<?> element) {
        return new JsonPrimitive(element.name());
    }

    /**
     * Transform from the json element
     * into the element
     *
     * @param element the json element
     * @return the element
     */
    @Override
    public Enum<?> resolve(final JsonElement element) {
        throw new UnsupportedOperationException("Cannot resolve from simple transformer");
    }
}
