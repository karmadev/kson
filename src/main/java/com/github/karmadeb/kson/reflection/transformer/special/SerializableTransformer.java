package com.github.karmadeb.kson.reflection.transformer.special;

import com.github.karmadeb.kson.annotation.deserialization.JsonConstructor;
import com.github.karmadeb.kson.annotation.deserialization.JsonParameter;
import com.github.karmadeb.kson.annotation.serialization.JsonInclude;
import com.github.karmadeb.kson.annotation.serialization.JsonSerializable;
import com.github.karmadeb.kson.element.JsonElement;
import com.github.karmadeb.kson.element.JsonObject;
import com.github.karmadeb.kson.exception.reflection.JsonSerializationException;
import com.github.karmadeb.kson.reflection.JsonSerializer;
import com.github.karmadeb.kson.reflection.ObjectTransformer;
import com.github.karmadeb.kson.reflection.transformer.TransformerUtils;

import java.lang.reflect.*;
import java.util.*;

/**
 * Represents a transformer for an object
 * which implements the {@link JsonSerializable} annotation
 * or a simple object if the transform instantiated
 * object supports it
 */
public class SerializableTransformer implements ObjectTransformer<Object> {

    /**
     * Transform the element into the
     * json element
     *
     * @param element the element
     * @return the json element
     */
    @Override
    public JsonElement transform(final Object element) {
        try {
            Class<?> elementClass = element.getClass();
            Constructor<? extends JsonSerializer<?>> constructor = findConstructor(elementClass);
            JsonSerializer<?> instance = constructor.newInstance(element);

            instance.process();
            return instance.serialize();
        } catch (ReflectiveOperationException ex) {
            throw new JsonSerializationException("Failed to transform instance", ex);
        }
    }

    private static Constructor<? extends JsonSerializer<?>> findConstructor(Class<?> elementClass) throws NoSuchMethodException {
        if (!elementClass.isAnnotationPresent(JsonSerializable.class))
            throw new JsonSerializationException("Cannot serialize " + elementClass.getSimpleName() + " because it's not serializable");

        JsonSerializable serializableMeta = elementClass.getAnnotation(JsonSerializable.class);
        Class<? extends JsonSerializer<?>> serializerType = serializableMeta.serializer();

        return serializerType.getDeclaredConstructor(Object.class);
    }

    /**
     * Transform from the json element
     * into the element
     *
     * @param element the json element
     * @return the element
     */
    @Override
    public Object resolve(final JsonElement element) {
        JsonObject object = element.getAsObject();

        String rawClass = object.getAsString("$class");
        long serialId = object.getAsLong("$serial");

        try {
            Class<?> clazz = Class.forName(rawClass);
            Constructor<?> matchingConstructor = tryObtainConstructor(clazz, serialId);

            if (matchingConstructor == null)
                throw new JsonSerializationException("Failed to locate a suitable constructor for class " + clazz.getCanonicalName());

            Map<String, MethodParameter> mappedMethodParameters = mapParameters(matchingConstructor, object);
            int parameterCount = countParams(mappedMethodParameters);

            Object[] parameters = new Object[parameterCount];
            Iterator<Map.Entry<String, MethodParameter>> entryIterator = mappedMethodParameters.entrySet().iterator();
            while (entryIterator.hasNext()) {
                Map.Entry<String, MethodParameter> entry = entryIterator.next();
                MethodParameter value = entry.getValue();

                MethodParameterMeta meta = value.meta;
                if (meta == null || meta.parameter == null)
                    continue;

                entryIterator.remove();
                JsonParameter param = meta.parameter;
                Class<?> expectedType = param.expectedType();
                if (expectedType == null)
                    expectedType = Object.class;

                Object raw = value.resolved;
                if (raw == null || expectedType.equals(Object.class)) {
                    parameters[meta.index] = raw;
                    continue;
                }

                Class<?> rawType = raw.getClass();
                if (param.expectedType().isArray() && (!rawType.isArray() && !Iterable.class.isAssignableFrom(rawType)))
                    throw new JsonSerializationException("Expected field " + param.fieldName() + " on " + clazz.getCanonicalName() + " constructor (serial " + serialId + ") to be an array, but found " + rawType.getCanonicalName() + " instead");

                if (param.expectedType().isArray()) {
                    if (Iterable.class.isAssignableFrom(rawType)) {
                        List<Object> arrayList = new ArrayList<>();
                        Iterable<?> iterator = (Iterable<?>) raw;

                        for (Object obj : iterator) {
                            arrayList.add(obj);
                        }

                        Object typeArray = Array.newInstance(param.expectedType().getComponentType(), arrayList.size());
                        for (int i = 0; i < arrayList.size(); i++)
                            Array.set(typeArray, i, arrayList.get(i));

                        parameters[meta.index] = typeArray;
                    } else {
                        int length = Array.getLength(raw);

                        Object typeArray = Array.newInstance(param.expectedType().getComponentType(), length);
                        for (int i = 0; i < length; i++)
                            Array.set(typeArray, i, Array.get(raw, i));

                        parameters[meta.index] = typeArray;
                    }
                } else {
                    parameters[meta.index] = raw;
                }
            }

            boolean initialAccessible = matchingConstructor.isAccessible();
            matchingConstructor.setAccessible(true);
            Object instance = matchingConstructor.newInstance(parameters);
            matchingConstructor.setAccessible(initialAccessible);

            mapInstanceFields(instance, mappedMethodParameters);

            return instance;
        } catch (ReflectiveOperationException ex) {
            throw new JsonSerializationException("Failed to resolve " + element.toJsonString() + " to " + rawClass, ex);
        }
    }

    private static int countParams(final Map<String, MethodParameter> params) {
        int cnt = 0;
        for (MethodParameter param : params.values()) {
            if (param != null && param.meta != null)
                cnt++;
        }

        return cnt;
    }

    private static Map<String, MethodParameter> mapParameters(final Constructor<?> matchingConstructor, final JsonObject object) {
        Map<String, MethodParameterMeta> nameIndexes = mapMethodParameters(matchingConstructor);
        Map<String, Object> resolved = resolveParameterValues(object);

        Map<String, MethodParameter> parameterMap = new HashMap<>();
        for (Map.Entry<String, Object> entry : resolved.entrySet()) {
            String key = entry.getKey();
            if (key == null) continue;

            Object rawValue = entry.getValue();

            MethodParameterMeta meta = nameIndexes.get(key);
            if (meta == null) {
                parameterMap.put(key, new MethodParameter(
                        null,
                        rawValue
                ));
                continue;
            }

            parameterMap.put(key, new MethodParameter(meta, rawValue));
        }

        return parameterMap;
    }

    private static Constructor<?> tryObtainConstructor(final Class<?> clazz, final long expectedSerialId) {
        Constructor<?> matchingConstructor = null;
        try {
            matchingConstructor = clazz.getDeclaredConstructor();
        } catch (NoSuchMethodException ignored) {}

        for (Constructor<?> constructor : clazz.getDeclaredConstructors()) {
            if (!constructor.isAnnotationPresent(JsonConstructor.class))
                continue;

            JsonConstructor jsonConstructor = constructor.getAnnotation(JsonConstructor.class);
            long serial = jsonConstructor.serialId();

            if (serial != expectedSerialId) continue;
            matchingConstructor = constructor;
            break;
        }

        return matchingConstructor;
    }

    private static void mapInstanceFields(final Object instance, final Map<String, MethodParameter> resolved) {
        for (Map.Entry<String, MethodParameter> entry : resolved.entrySet()) {
            String name = entry.getKey();
            MethodParameter value = entry.getValue();

            Field field;
            try {
                field = instance.getClass().getDeclaredField(name);
            } catch (NoSuchFieldException ex) {
                field = findField(instance, name);
            }

            Object rawValue = value.resolved;

            if (field.getType().isArray()) {
                Object[] res = (Object[]) rawValue;
                Class<?> realType = field.getType().getComponentType();

                Object array = Array.newInstance(realType, res.length);
                for (int i = 0; i < res.length; i++) {
                    Array.set(array, i, realType.cast(res[i]));
                }

                rawValue = array;
            }

            boolean initialAccessible = field.isAccessible();
            field.setAccessible(true);
            try {
                try {
                    Field modifiersField = Field.class.getDeclaredField("modifiers");
                    modifiersField.setAccessible(true);
                    modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
                    int modifiers = field.getModifiers();

                    field.set(instance, rawValue);

                    modifiersField.setInt(field, modifiers);
                    modifiersField.setAccessible(false);
                } catch (ReflectiveOperationException ignored) {
                    field.set(instance, rawValue);
                }
            } catch (ReflectiveOperationException ignored2) {}

            field.setAccessible(initialAccessible);
        }
    }

    private static Field findField(final Object instance, final String name) {
        Field[] fields = instance.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (!field.isAnnotationPresent(JsonInclude.class)) continue;

            JsonInclude include = field.getAnnotation(JsonInclude.class);
            if (include.name().equals(name))
                return field;
        }

        throw new JsonSerializationException("Failed to find matching field: " + name);
    }

    private static Map<String, Object> resolveParameterValues(final JsonObject object) {
        Map<String, Object> parameterNames = new HashMap<>();
        for (String key : object.getKeys()) {
            if (key.equalsIgnoreCase("$class") || key.equalsIgnoreCase("$serial"))
                continue;

            JsonElement value = object.get(key);
            if (!value.isObject()) continue;

            JsonObject valueObject = value.getAsObject();
            Object resolved = TransformerUtils.resolve(valueObject);
            parameterNames.put(key, resolved);
        }

        return parameterNames;
    }

    private static Map<String, MethodParameterMeta> mapMethodParameters(final Constructor<?> matchingConstructor) {
        Map<String, MethodParameterMeta> nameIndexes = new HashMap<>();
        Parameter[] methodParams = matchingConstructor.getParameters();
        for (int i = 0; i < methodParams.length; i++) {
            Parameter parameter = methodParams[i];
            if (!parameter.isAnnotationPresent(JsonParameter.class))
                throw new JsonSerializationException("Found json constructor with a parameter with no json parameter data");

            JsonParameter jsonParameter = parameter.getAnnotation(JsonParameter.class);
            String name = jsonParameter.fieldName();

            nameIndexes.put(name, new MethodParameterMeta(jsonParameter, i));
        }

        return nameIndexes;
    }

    private static final class MethodParameterMeta {

        public final JsonParameter parameter;
        public final int index;

        public MethodParameterMeta(final JsonParameter parameter, final int index) {
            this.parameter = parameter;
            this.index = index;
        }
    }

    private static final class MethodParameter {

        private final MethodParameterMeta meta;
        private final Object resolved;

        public MethodParameter(final MethodParameterMeta meta, final Object resolved) {
            this.meta = meta;
            this.resolved = resolved;
        }
    }
}
