package com.github.karmadeb.kson.reflection.transformer.collection;

import com.github.karmadeb.kson.element.JsonArray;
import com.github.karmadeb.kson.element.JsonElement;
import com.github.karmadeb.kson.element.JsonObject;
import com.github.karmadeb.kson.reflection.ObjectTransformer;
import com.github.karmadeb.kson.reflection.transformer.TransformerUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Represents a collection transformer
 */
public final class CollectionTransformer implements ObjectTransformer<Iterable<?>> {

    /**
     * Transform the element into the
     * json element
     *
     * @param element the element
     * @return the json element
     */
    @Override
    public JsonElement transform(final Iterable<?> element) {
        JsonObject object = new JsonObject();
        object.set("$transformer", CollectionTransformer.class.getCanonicalName());

        JsonArray array = new JsonArray();

        int index = 0;
        for (Object content : element) {
            JsonObject collectionObject = new JsonObject();
            collectionObject.set("$index", index++);
            collectionObject.set("$value", TransformerUtils.transform(content));

            array.add(collectionObject);
        }

        object.set("$value", array);
        return object;
    }

    /**
     * Transform from the json element
     * into the element
     *
     * @param element the json element
     * @return the element
     */
    @Override
    public List<?> resolve(final JsonElement element) {
        if (!element.isArray())
            return Collections.emptyList();

        JsonArray jsonArray = element.getAsArray();

        List<Object> values = new ArrayList<>(jsonArray.size());
        for (JsonElement arrayElement : jsonArray) {
            if (!arrayElement.isObject()) continue;

            JsonObject elementObject = arrayElement.getAsObject();
            if (!elementObject.has("$index") || !elementObject.has("$value")) continue;

            JsonElement indexElement = elementObject.get("$index");
            JsonElement valueElement = elementObject.get("$value");
            if (!indexElement.isNumber() || !valueElement.isObject()) continue;

            int index = indexElement.getAsInteger();
            JsonObject value = valueElement.getAsObject();

            Object transformed = TransformerUtils.resolve(value);
            values.add(index, transformed);
        }

        return values;
    }
}
