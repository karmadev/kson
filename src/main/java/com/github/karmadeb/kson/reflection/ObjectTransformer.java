package com.github.karmadeb.kson.reflection;

import com.github.karmadeb.kson.element.JsonElement;

/**
 * Represents an object transformer. The object
 * transformer allows to convert from any object
 * into a json element
 * @param <TypeFrom> the object to transform to
 */
public interface ObjectTransformer<TypeFrom> {

    /**
     * Transform the element into the
     * json element
     *
     * @param element the element
     * @return the json element
     */
    JsonElement transform(final TypeFrom element);

    /**
     * Transform from the json element
     * into the element
     *
     * @param element the json element
     * @return the element
     */
    TypeFrom resolve(final JsonElement element);
}