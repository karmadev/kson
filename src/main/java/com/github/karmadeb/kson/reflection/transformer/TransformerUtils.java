package com.github.karmadeb.kson.reflection.transformer;

import com.github.karmadeb.kson.annotation.serialization.JsonSerializable;
import com.github.karmadeb.kson.element.JsonElement;
import com.github.karmadeb.kson.element.JsonObject;
import com.github.karmadeb.kson.element.JsonPrimitive;
import com.github.karmadeb.kson.exception.reflection.JsonSerializationException;
import com.github.karmadeb.kson.reflection.ObjectTransformer;
import com.github.karmadeb.kson.reflection.transformer.collection.ArrayTransformer;
import com.github.karmadeb.kson.reflection.transformer.collection.CollectionTransformer;
import com.github.karmadeb.kson.reflection.transformer.collection.map.MapEntryTransformer;
import com.github.karmadeb.kson.reflection.transformer.collection.map.MapTransformer;
import com.github.karmadeb.kson.reflection.transformer.primitive.BooleanTransformer;
import com.github.karmadeb.kson.reflection.transformer.primitive.CharSequenceTransformer;
import com.github.karmadeb.kson.reflection.transformer.primitive.NumberTransformer;
import com.github.karmadeb.kson.reflection.transformer.simple.collection.SimpleArrayTransformer;
import com.github.karmadeb.kson.reflection.transformer.simple.collection.SimpleCollectionTransformer;
import com.github.karmadeb.kson.reflection.transformer.simple.collection.map.SimpleMapEntryTransformer;
import com.github.karmadeb.kson.reflection.transformer.simple.collection.map.SimpleMapTransformer;
import com.github.karmadeb.kson.reflection.transformer.simple.primitive.SimpleBooleanTransformer;
import com.github.karmadeb.kson.reflection.transformer.simple.primitive.SimpleCharSequenceTransformer;
import com.github.karmadeb.kson.reflection.transformer.simple.primitive.SimpleNumberTransformer;
import com.github.karmadeb.kson.reflection.transformer.simple.special.SimpleEnumTransformer;
import com.github.karmadeb.kson.reflection.transformer.simple.special.SimpleSerializableTransformer;
import com.github.karmadeb.kson.reflection.transformer.special.EnumTransformer;
import com.github.karmadeb.kson.reflection.transformer.special.SerializableTransformer;

import java.lang.reflect.Constructor;
import java.util.Map;

/**
 * Transformer utilities
 */
public final class TransformerUtils {

    private TransformerUtils() {}

    /**
     * Gets a transformer for the specified
     * object
     *
     * @param object the object to obtain the transformer
     *               from
     * @return the object transformer
     * @throws JsonSerializationException if there's no suitable transformer for the
     * specified object type
     */
    public static ObjectTransformer<Object> getTransformerFromObject(final Object object) throws JsonSerializationException {
        if (object == null)
            return null;

        return getTransformerFromType(object.getClass());
    }

    /**
     * Get a transformer for the specified type
     *
     * @param type the object type
     * @return the object transformer
     * @throws JsonSerializationException if there's no suitable transformer for the
     * specified object type
     */
    @SuppressWarnings("unchecked")
    public static ObjectTransformer<Object> getTransformerFromType(final Class<?> type) throws JsonSerializationException {
        ObjectTransformer<?> transformer;
        if (CharSequence.class.isAssignableFrom(type)) {
            transformer = new CharSequenceTransformer();
        } else if (Boolean.class.isAssignableFrom(type) || boolean.class.isAssignableFrom(type)) {
            transformer = new BooleanTransformer();
        } else if (Number.class.isAssignableFrom(type) || type.isPrimitive()) {
            transformer = new NumberTransformer();
        } else if (type.isEnum()) {
            transformer = new EnumTransformer();
        } else if (Map.class.isAssignableFrom(type)) {
            transformer = new MapTransformer();
        } else if (Map.Entry.class.isAssignableFrom(type)) {
            transformer = new MapEntryTransformer();
        } else if (Iterable.class.isAssignableFrom(type)) {
            transformer = new CollectionTransformer();
        } else if (type.isArray()) {
            transformer = new ArrayTransformer();
        } else {
            if (type.isAnnotationPresent(JsonSerializable.class)) {
                transformer = new SerializableTransformer();
            } else {
                throw new JsonSerializationException("Cannot locate default transformer for type " + type.getSimpleName());
            }
        }

        return (ObjectTransformer<Object>) transformer;
    }

    /**
     * Converse an object into a json
     * element
     *
     * @param object the object to converse
     * @return the conversed object
     * @throws JsonSerializationException if there's a problem while trying to serialize the object
     */
    public static JsonElement transform(final Object object) throws JsonSerializationException {
        ObjectTransformer<Object> transformer = getTransformerFromObject(object);
        if (transformer == null)
            return JsonPrimitive.createNull();

        return transformer.transform(object);
    }

    /**
     * Gets a transformer for the specified
     * object
     *
     * @param object the object to obtain the transformer
     *               from
     * @return the object transformer
     * @throws JsonSerializationException if there's no suitable transformer for the
     * specified object type
     */
    public static ObjectTransformer<Object> getSimpleTransformerFromObject(final Object object) throws JsonSerializationException {
        if (object == null)
            return null;

        return getSimpleTransformerFromType(object.getClass());
    }

    /**
     * Get a transformer for the specified type
     *
     * @param type the object type
     * @return the object transformer
     * @throws JsonSerializationException if there's no suitable transformer for the
     * specified object type
     */
    @SuppressWarnings("unchecked")
    public static ObjectTransformer<Object> getSimpleTransformerFromType(final Class<?> type) throws JsonSerializationException {
        ObjectTransformer<?> transformer;
        if (CharSequence.class.isAssignableFrom(type)) {
            transformer = new SimpleCharSequenceTransformer();
        } else if (Boolean.class.isAssignableFrom(type) || boolean.class.isAssignableFrom(type)) {
            transformer = new SimpleBooleanTransformer();
        } else if (Number.class.isAssignableFrom(type) || type.isPrimitive()) {
            transformer = new SimpleNumberTransformer();
        } else if (type.isEnum()) {
            transformer = new SimpleEnumTransformer();
        } else if (Map.class.isAssignableFrom(type)) {
            transformer = new SimpleMapTransformer();
        } else if (Map.Entry.class.isAssignableFrom(type)) {
            transformer = new SimpleMapEntryTransformer();
        } else if (Iterable.class.isAssignableFrom(type)) {
            transformer = new SimpleCollectionTransformer();
        } else if (type.isArray()) {
            transformer = new SimpleArrayTransformer();
        } else {
            if (type.isAnnotationPresent(JsonSerializable.class)) {
                transformer = new SimpleSerializableTransformer();
            } else {
                throw new JsonSerializationException("Cannot locate default transformer for type " + type.getSimpleName());
            }
        }

        return (ObjectTransformer<Object>) transformer;
    }

    /**
     * Converse an object into a json
     * element
     *
     * @param object the object to converse
     * @return the conversed object
     * @throws JsonSerializationException if there's a problem while trying to serialize the object
     */
    public static JsonElement simpleTransform(final Object object) throws JsonSerializationException {
        ObjectTransformer<Object> transformer = getSimpleTransformerFromObject(object);
        if (transformer == null)
            return JsonPrimitive.createNull();

        return transformer.transform(object);
    }

    /**
     * Resolves a json element into its object
     *
     * @param element the serialized json element
     * @return the object
     * @throws JsonSerializationException if there's a problem while trying to resolve the object
     */
    public static Object resolve(final JsonElement element) throws JsonSerializationException {
        if (!element.isObject())
            throw new JsonSerializationException("Cannot resolve from non-object element");

        JsonObject object = element.getAsObject();
        if (!object.has("$transformer") && object.has("$class") &&
                object.has("$serial")) {
            SerializableTransformer transformer = new SerializableTransformer();
            return transformer.resolve(object);
        }

        if (!object.has("$transformer") || !object.has("$value"))
            throw new JsonSerializationException("Missing transformer and/or value from " + object.toJsonString());

        String transformer = object.getAsString("$transformer");
        JsonElement value = object.get("$value");
        if (object.has("$class"))
            value = object; //it's an enum, we need the whole object

        ObjectTransformer<Object> resolver = getTransformer(transformer);
        return resolver.resolve(value);
    }

    /**
     * Resolves a json element into its object
     *
     * @param element the serialized json element
     * @param type the expected object type
     * @return the object
     * @param <T> the object type
     * @throws JsonSerializationException if there's a problem while trying to resolve the object
     */
    public static <T> T resolve(final JsonElement element, final Class<T> type) throws JsonSerializationException {
        Object resolved = resolve(element);
        try {
            return type.cast(resolved);
        } catch (ClassCastException ex) {
            throw new JsonSerializationException("Cannot resolve " + type + " from " + element.toJsonString());
        }
    }

    @SuppressWarnings("unchecked")
    private static ObjectTransformer<Object> getTransformer(final String transformerClass) {
        try {
            Class<? extends ObjectTransformer<?>> transformer = (Class<? extends ObjectTransformer<?>>) Class.forName(transformerClass)
                    .asSubclass(ObjectTransformer.class);

            Constructor<? extends ObjectTransformer<?>> constructor = transformer.getDeclaredConstructor();
            return (ObjectTransformer<Object>) constructor.newInstance();
        } catch (ReflectiveOperationException ex) {
            throw new JsonSerializationException("Failed to create transformer " + transformerClass, ex);
        }
    }
}
