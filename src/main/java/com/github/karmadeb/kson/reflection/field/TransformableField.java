package com.github.karmadeb.kson.reflection.field;

import com.github.karmadeb.kson.element.JsonElement;
import com.github.karmadeb.kson.element.JsonPrimitive;
import com.github.karmadeb.kson.reflection.ObjectTransformer;

import java.lang.reflect.Field;

/**
 * Represents a simple field of an
 * object
 */
public final class TransformableField extends SimpleField {

    private final ObjectTransformer<Object> transformer;

    /**
     * Creates a simple field
     *
     * @param object the field object
     * @param field the object field
     * @param transformer the field transformer
     * @throws ReflectiveOperationException if there's a problem while mapping
     * the field
     * @deprecated see {@link #TransformableField(Object, Field, String, ObjectTransformer)} with null name
     */
    @SuppressWarnings("unused")
    @Deprecated
    public TransformableField(final Object object, final Field field, final ObjectTransformer<?> transformer) throws ReflectiveOperationException {
        this(object, field, null, transformer);
    }

    /**
     * Creates a simple field
     *
     * @param object the field object
     * @param field the object field
     * @param name the field name
     * @param transformer the field transformer
     * @throws ReflectiveOperationException if there's a problem while mapping
     * the field
     */
    @SuppressWarnings("unchecked")
    public TransformableField(final Object object, final Field field, final String name, final ObjectTransformer<?> transformer) throws ReflectiveOperationException {
        super(object, field, name);
        this.transformer = (ObjectTransformer<Object>) transformer;
    }

    /**
     * Get the transformed value
     *
     * @return the transformed value
     */
    public JsonElement getTransformed() {
        if (this.value == null)
            return JsonPrimitive.createNull();

        return this.transformer.transform(this.value);
    }

    /**
     * Sets the field value
     *
     * @param element the element to set the value to
     * @throws ReflectiveOperationException if the value fails to set
     */
    public void set(final JsonElement element) throws ReflectiveOperationException {
        Object result = transformer.resolve(element);
        super.set(result);
    }

    /**
     * Sets the field value
     *
     * @param element the element to set the value to
     * @throws ReflectiveOperationException if the value fails to set
     */
    @Override
    public void set(final Object element) throws ReflectiveOperationException {
        if (element instanceof JsonElement) {
            this.set((JsonElement) element);
            return;
        }

        super.set(element);
    }
}