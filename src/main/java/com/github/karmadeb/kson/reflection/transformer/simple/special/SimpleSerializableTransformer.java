package com.github.karmadeb.kson.reflection.transformer.simple.special;

import com.github.karmadeb.kson.annotation.serialization.JsonSerializable;
import com.github.karmadeb.kson.element.JsonElement;
import com.github.karmadeb.kson.exception.reflection.JsonSerializationException;
import com.github.karmadeb.kson.reflection.JsonSerializer;
import com.github.karmadeb.kson.reflection.ObjectTransformer;

import java.lang.reflect.Constructor;

/**
 * Represents a transformer for an object
 * which implements the {@link JsonSerializable} annotation
 * or a simple object if the transform instantiated
 * object supports it
 */
public class SimpleSerializableTransformer implements ObjectTransformer<Object> {

    /**
     * Transform the element into the
     * json element
     *
     * @param element the element
     * @return the json element
     */
    @Override
    public JsonElement transform(final Object element) {
        try {
            Class<?> elementClass = element.getClass();
            Constructor<? extends JsonSerializer<?>> constructor = findConstructor(elementClass);
            JsonSerializer<?> instance = constructor.newInstance(element);

            instance.setSimple(true);
            instance.process();
            return instance.serialize();
        } catch (ReflectiveOperationException ex) {
            throw new JsonSerializationException("Failed to transform instance", ex);
        }
    }

    private static Constructor<? extends JsonSerializer<?>> findConstructor(Class<?> elementClass) throws NoSuchMethodException {
        if (!elementClass.isAnnotationPresent(JsonSerializable.class))
            throw new JsonSerializationException("Cannot serialize " + elementClass.getSimpleName() + " because it's not serializable");

        JsonSerializable serializableMeta = elementClass.getAnnotation(JsonSerializable.class);
        Class<? extends JsonSerializer<?>> serializerType = serializableMeta.serializer();

        return serializerType.getDeclaredConstructor(Object.class);
    }

    /**
     * Transform from the json element
     * into the element
     *
     * @param element the json element
     * @return the element
     */
    @Override
    public Object resolve(final JsonElement element) {
        throw new UnsupportedOperationException("Cannot resolve from simple transformer");
    }
}
