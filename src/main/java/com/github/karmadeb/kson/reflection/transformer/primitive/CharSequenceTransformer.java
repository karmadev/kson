package com.github.karmadeb.kson.reflection.transformer.primitive;

import com.github.karmadeb.kson.element.JsonElement;
import com.github.karmadeb.kson.element.JsonObject;
import com.github.karmadeb.kson.reflection.ObjectTransformer;

/**
 * Represents a char sequence transformer
 */
public final class CharSequenceTransformer implements ObjectTransformer<CharSequence> {

    /**
     * Transform the element into the
     * json element
     *
     * @param element the element
     * @return the json element
     */
    @Override
    public JsonElement transform(final CharSequence element) {
        JsonObject object = new JsonObject();
        object.set("$transformer", CharSequenceTransformer.class.getCanonicalName());
        object.set("$value", element);

        return object;
    }

    /**
     * Transform from the json element
     * into the element
     *
     * @param element the json element
     * @return the element
     */
    @Override
    public CharSequence resolve(final JsonElement element) {
        if (!element.isString())
            return "";

        return element.getAsString();
    }
}
