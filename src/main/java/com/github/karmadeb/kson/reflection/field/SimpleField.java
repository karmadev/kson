package com.github.karmadeb.kson.reflection.field;

import com.github.karmadeb.kson.annotation.serialization.JsonInclude;

import java.lang.reflect.Field;

/**
 * Represents a simple field of an
 * object
 */
public class SimpleField {

    protected final Object object;
    protected final Field field;

    protected final String name;
    protected final Object value;

    /**
     * Creates a simple field
     *
     * @param object the field object
     * @param field the object field
     * @param name the field name
     * @throws ReflectiveOperationException if there's a problem while mapping
     * the field
     */
    public SimpleField(final Object object, final Field field, final String name) throws ReflectiveOperationException {
        this.object = object;
        this.field = field;

        boolean initialAccessible = field.isAccessible();
        field.setAccessible(true);
        this.value = field.get(object instanceof Class<?> ? null : object);
        field.setAccessible(initialAccessible);

        this.name = name == null ? getFieldName(field) : name;
    }

    /**
     * Get the field name
     *
     * @return the name of the field
     */
    public String getName() {
        return this.name;
    }

    /**
     * Get the field value
     *
     * @return the value of the field
     */
    public Object getValue() {
        return this.value;
    }

    /**
     * Sets the field value
     *
     * @param element the element to set the value to
     * @throws ReflectiveOperationException if the value fails to set
     */
    public void set(final Object element) throws ReflectiveOperationException {
        boolean initialAccessible = this.field.isAccessible();
        this.field.setAccessible(true);
        this.field.set(object, element);
        this.field.setAccessible(initialAccessible);
    }

    private static String getFieldName(final Field field) {
        String name = "";
        if (field.isAnnotationPresent(JsonInclude.class)) {
            JsonInclude annotation = field.getAnnotation(JsonInclude.class);
            name = annotation.name();
        }

        if (name.trim().isEmpty() || name.equalsIgnoreCase("%default%"))
            name = field.getName();

        return name;
    }
}