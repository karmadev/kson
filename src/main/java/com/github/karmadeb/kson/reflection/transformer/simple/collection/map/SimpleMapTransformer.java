package com.github.karmadeb.kson.reflection.transformer.simple.collection.map;

import com.github.karmadeb.kson.element.JsonArray;
import com.github.karmadeb.kson.element.JsonElement;
import com.github.karmadeb.kson.element.JsonObject;
import com.github.karmadeb.kson.reflection.ObjectTransformer;

import java.util.Map;

/**
 * Represents a map (key = value object) transformer
 */
public final class SimpleMapTransformer implements ObjectTransformer<Map<?, ?>> {

    /**
     * Transform the element into the
     * json element
     *
     * @param kvMap the element
     * @return the json element
     */
    @Override
    public JsonArray transform(final Map<?, ?> kvMap) {
        JsonArray jsonArray = new JsonArray();
        for (Map.Entry<?, ?> entry : kvMap.entrySet()) {
            SimpleMapEntryTransformer entryTransformer = new SimpleMapEntryTransformer();
            JsonObject entryJsonObject = entryTransformer.transform(entry);

            jsonArray.add(entryJsonObject);
        }

        return jsonArray;
    }

    /**
     * Transform from the json element
     * into the element
     *
     * @param element the json element
     * @return the element
     */
    @Override
    public Map<?, ?> resolve(final JsonElement element) {
        throw new UnsupportedOperationException("Cannot resolve from simple transformer");
    }
}
