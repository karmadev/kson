package com.github.karmadeb.kson.reflection.transformer.simple.collection.map;

import com.github.karmadeb.kson.element.JsonElement;
import com.github.karmadeb.kson.element.JsonObject;
import com.github.karmadeb.kson.reflection.ObjectTransformer;
import com.github.karmadeb.kson.reflection.transformer.TransformerUtils;

import java.util.Map;

/**
 * Represents a map entry (key-value object) transformer
 */
public class SimpleMapEntryTransformer implements ObjectTransformer<Map.Entry<?, ?>> {

    /**
     * Transform the element into the
     * json element
     *
     * @param entry the element
     * @return the json element
     */
    @Override
    public JsonObject transform(final Map.Entry<?, ?> entry) {
        JsonObject jsonObject = new JsonObject();

        Object key = entry.getKey();
        Object value = entry.getValue();

        JsonElement k = TransformerUtils.transform(key);
        JsonElement v = TransformerUtils.transform(value);
        jsonObject.set("key", k);
        jsonObject.set("value", v);

        return jsonObject;
    }

    /**
     * Transform from the json element
     * into the element
     *
     * @param element the json element
     * @return the element
     */
    @Override
    public Map.Entry<?, ?> resolve(final JsonElement element) {
        throw new UnsupportedOperationException("Cannot resolve from simple transformer");
    }
}
