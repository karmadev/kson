package com.github.karmadeb.kson.reflection.transformer.simple.primitive;

import com.github.karmadeb.kson.element.JsonElement;
import com.github.karmadeb.kson.element.JsonPrimitive;
import com.github.karmadeb.kson.reflection.ObjectTransformer;

/**
 * Represents a number transformer
 */
public final class SimpleNumberTransformer implements ObjectTransformer<Number> {

    /**
     * Transform the element into the
     * json element
     *
     * @param element the element
     * @return the json element
     */
    @Override
    public JsonElement transform(final Number element) {
        if (element == null)
            return JsonPrimitive.createNull();

        return new JsonPrimitive(element);
    }

    /**
     * Transform from the json element
     * into the element
     *
     * @param element the json element
     * @return the element
     */
    @Override
    public Number resolve(final JsonElement element) {
        throw new UnsupportedOperationException("Cannot resolve from simple transformer");
    }
}
