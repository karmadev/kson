package com.github.karmadeb.kson.printer;

import com.github.karmadeb.kson.element.JsonElement;
import com.github.karmadeb.kson.element.JsonPrimitive;

import java.io.Writer;

/**
 * Represents a json element printer
 * @param <T> the element type
 */
@SuppressWarnings("unused")
public abstract class JsonPrinter<T extends JsonElement> {

    protected final T element;

    protected int indentation;
    protected char indentationCharacter = '\t';

    /**
     * Initialize the json printer
     *
     * @param element the element to print
     */
    public JsonPrinter(final T element) {
        this.element = element;
    }

    /**
     * Set the printer indentation level
     *
     * @param indentation the indentation
     * @return the printer
     */
    public final JsonPrinter<T> withIndent(final int indentation) {
        this.indentation = Math.max(0, indentation);
        return this;
    }

    /**
     * Set the printer indentation character
     *
     * @param character the character to use
     *                  when indentation
     * @return the printer
     */
    public final JsonPrinter<T> withIndentChar(final char character) {
        this.indentationCharacter = character;
        return this;
    }

    /**
     * Print the element
     *
     * @param level the object current level
     * @param out the output
     */
    public void print(final int level, final Writer out) {
        this.print(level, false, false, out);
    }

    /**
     * Reset the printer
     */
    public abstract void reset();

    /**
     * Print the element
     *
     * @param level the object current level
     * @param key include the object key
     * @param coma write coma at the end of
     *             the object
     * @param out the output
     */
    public abstract void print(final int level, final boolean key, final boolean coma, final Writer out);

    protected final String primitiveToString(final JsonPrimitive primitive) {
        if (primitive.isString()) {
            return String.format("\"%s\"", primitive.getAsString()
                    .replace("\"", "\\\"")
                    .replace("\n", "\\\\n"));
        }

        return primitive.toString();
    }

    protected final String createKeyOpen(final char key) {
        if (this.element.getKey().isEmpty())
            return String.valueOf(key);

        return String.format("\"%s\": %s",
                this.element.getKey().replace("\"",
                        "\\\""),
                key);
    }

    protected final String createCloseIndent(final int level) {
        if (level == 1)
            return "";

        StringBuilder builder = new StringBuilder();
        int sub = level - this.indentation;
        int abc = sub % this.indentation;

        if (abc > 0)
            sub += abc;

        System.out.println(sub + " | " + level + " | " + this.indentation);

        for (int i = 0; i < this.indentation + sub; i++) {
            builder.append(this.indentationCharacter);
        }

        return builder.toString();
    }
}
