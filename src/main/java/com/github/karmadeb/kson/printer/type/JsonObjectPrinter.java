package com.github.karmadeb.kson.printer.type;

import com.github.karmadeb.kson.element.JsonElement;
import com.github.karmadeb.kson.element.JsonObject;
import com.github.karmadeb.kson.element.JsonPrimitive;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

/**
 * Json element to string printer
 */
public final class JsonObjectPrinter extends JsonTypePrinter<JsonObject> {

    /**
     * Initialize the json printer
     *
     * @param element the element to print
     */
    public JsonObjectPrinter(final JsonObject element) {
        super(element);
    }

    /**
     * Reset the printer
     */
    @Override
    public void reset() {
        this.builder.reset();
    }

    /**
     * Print the element
     *
     * @param level the object current level
     * @param key include the object key
     * @param out the output
     */
    @Override
    public void print(final int level, final boolean key, final boolean coma, final Writer out) {
        builder.setIndentationLevel(this.indentation);
        builder.setIndentationChar(this.indentationCharacter);

        if (key) {
            builder.append(createKeyOpen('{'));
        } else {
            builder.append("{");
        }
        builder.incrementLevel();

        int index = 0;
        final int max = this.element.size();

        for (String eKey : this.element.getKeys()) {
            JsonElement objectElement = this.element.get(eKey);
            if (objectElement.isPrimitive()) {
                JsonPrimitive primitive = objectElement.getAsPrimitive();
                builder.append(String.format("\"%s\": %s",
                        primitive.getKey().replace("\"", "\\\""),
                        primitiveToString(primitive)), index++ != max - 1);
                continue;
            }

            JsonTypePrinter<?> printer = (JsonTypePrinter<?>) objectElement.getJsonPrinter();
            printer.setBuilder(builder);

            StringWriter writer = new StringWriter();
            printer.print(level + 1, true, index++ != max - 1, writer);
        }

        builder.decrementLevel();
        builder.append("}");
        if (coma)
            builder.prepend("}", ", ");

        String result = builder.toString();
        try {
            out.write(result);
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }
}