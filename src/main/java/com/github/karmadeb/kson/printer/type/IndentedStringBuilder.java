package com.github.karmadeb.kson.printer.type;

final class IndentedStringBuilder {

    private int initialIndentation;
    private char indentationChar;
    private int indentationLevel;

    private final StringBuilder builder = new StringBuilder();

    public void setIndentationLevel(final int level) {
        if (this.initialIndentation != 0)
            return;

        this.initialIndentation = level;
    }

    public void setIndentationChar(final char character) {
        if (this.indentationChar != '\0')
            return;

        this.indentationChar = character;
    }

    public void incrementLevel() {
        this.indentationLevel += this.initialIndentation;
    }

    public void decrementLevel() {
        this.indentationLevel -= this.initialIndentation;
    }

    public IndentedStringBuilder prepend(final String find, final String content) {
        int index = this.builder.lastIndexOf(find) + 1;
        this.builder.insert(index, content);
        return this;
    }

    public IndentedStringBuilder append(final String content) {
        return this.append(content, false);
    }

    public IndentedStringBuilder append(final String content, final boolean includeComa) {
        this.writeIndent();
        builder.append(content);
        if (includeComa)
            builder.append(", ");

        if (this.initialIndentation != 0)
            builder.append('\n');

        return this;
    }

    public void reset() {
        this.builder.setLength(0);
    }

    /**
     * Returns a string representation of the object. In general, the
     * {@code toString} method returns a string that
     * "textually represents" this object. The result should
     * be a concise but informative representation that is easy for a
     * person to read.
     * It is recommended that all subclasses override this method.
     * <p>
     * The {@code toString} method for class {@code Object}
     * returns a string consisting of the name of the class of which the
     * object is an instance, the at-sign character `{@code @}', and
     * the unsigned hexadecimal representation of the hash code of the
     * object. In other words, this method returns a string equal to the
     * value of:
     * <blockquote>
     * <pre>
     * getClass().getName() + '@' + Integer.toHexString(hashCode())
     * </pre></blockquote>
     *
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return this.builder.toString();
    }

    private void writeIndent() {
        if (this.indentationLevel == 0)
            return;

        int repeat = this.indentationLevel;
        while (repeat-- > 0)
            this.builder.append(indentationChar);
    }
}
