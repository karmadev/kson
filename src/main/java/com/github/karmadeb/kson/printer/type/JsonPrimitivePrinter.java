package com.github.karmadeb.kson.printer.type;

import com.github.karmadeb.kson.element.JsonPrimitive;
import com.github.karmadeb.kson.printer.JsonPrinter;

import java.io.IOException;
import java.io.Writer;

/**
 * Json element to string printer
 */
public final class JsonPrimitivePrinter extends JsonPrinter<JsonPrimitive> {

    /**
     * Initialize the json printer
     *
     * @param element the element to print
     */
    public JsonPrimitivePrinter(final JsonPrimitive element) {
        super(element);
    }

    /**
     * Reset the printer
     */
    @Override
    public void reset() {

    }

    /**
     * Print the element
     *
     * @param level the object current level
     * @param key include the object key
     * @param out the output
     */
    @Override
    public void print(final int level, final boolean key, final boolean coma, final Writer out) {
        String raw;
        if (key) {
            raw = String.format("\"%s\": %s", this.element.getKey()
                    .replace("\"", "\\\""), primitiveToString(this.element));
        } else {
            raw = primitiveToString(this.element);
        }

        try {
            out.write(raw);
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }
}