package com.github.karmadeb.kson.printer.type;

import com.github.karmadeb.kson.element.JsonElement;
import com.github.karmadeb.kson.printer.JsonPrinter;

abstract class JsonTypePrinter<T extends JsonElement> extends JsonPrinter<T> {

    protected IndentedStringBuilder builder = new IndentedStringBuilder();

    /**
     * Initialize the json printer
     *
     * @param element the element to print
     */
    public JsonTypePrinter(final T element) {
        super(element);
    }

    public IndentedStringBuilder getBuilder() {
        return this.builder;
    }

    public void setBuilder(final IndentedStringBuilder builder) {
        this.builder = builder;
    }
}
