package com.github.karmadeb.kson.printer.type;

import com.github.karmadeb.kson.element.JsonArray;
import com.github.karmadeb.kson.element.JsonElement;
import com.github.karmadeb.kson.element.JsonPrimitive;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

/**
 * Json element to string printer
 */
public final class JsonArrayPrinter extends JsonTypePrinter<JsonArray> {

    /**
     * Initialize the json printer
     *
     * @param element the element to print
     */
    public JsonArrayPrinter(final JsonArray element) {
        super(element);
    }

    /**
     * Reset the printer
     */
    @Override
    public void reset() {
        this.builder.reset();
    }

    /**
     * Print the element
     *
     * @param level the object current level
     * @param key include the object key
     * @param out the output
     */
    @Override
    public void print(final int level, final boolean key, final boolean coma, final Writer out) {
        builder.setIndentationLevel(this.indentation);
        builder.setIndentationChar(this.indentationCharacter);

        if (key) {
            builder.append(createKeyOpen('['));
        } else {
            builder.append("[");
        }
        builder.incrementLevel();

        final int max = this.element.size();

        for (int i = 0; i < max; i++) {
            JsonElement arrayElement = this.element.get(i);
            if (arrayElement.isPrimitive()) {
                JsonPrimitive primitive = arrayElement.getAsPrimitive();
                builder.append(primitiveToString(primitive), i != max - 1);
                continue;
            }

            JsonTypePrinter<?> printer = (JsonTypePrinter<?>) arrayElement.getJsonPrinter();
            printer.setBuilder(builder);

            StringWriter writer = new StringWriter();
            printer.print(level + 1, false, i != max - 1, writer);
        }

        builder.decrementLevel();
        builder.append("]");
        if (coma)
            builder.prepend("]", ", ");

        String result = builder.toString();
        try {
            out.write(result);
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }
}