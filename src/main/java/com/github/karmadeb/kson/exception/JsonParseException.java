package com.github.karmadeb.kson.exception;

/**
 * This exception is thrown when an
 * error occurs during json parsing
 */
public class JsonParseException extends RuntimeException {

    /**
     * Create the exception
     *
     * @param message the exception message
     */
    public JsonParseException(final String message) {
        super(message);
    }

    /**
     * Create the exception
     *
     * @param message the exception message
     * @param cause the exception cause
     */
    public JsonParseException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
