package com.github.karmadeb.kson.exception.reflection;

import com.github.karmadeb.kson.reflection.serialization.JsonClassSerializer;

/**
 * This exception is thrown when any
 * problem raises during a {@link JsonClassSerializer}
 * serialization
 */
public class JsonSerializationException extends RuntimeException {

    /**
     * Create a serialization exception
     *
     * @param message the error message
     */
    public JsonSerializationException(final String message) {
        super(message);
    }

    /**
     * Create a serialization exception
     *
     * @param message the error message
     * @param cause the error cause
     */
    public JsonSerializationException(final String message, final Throwable cause) {
        super(message, cause);
    }
}