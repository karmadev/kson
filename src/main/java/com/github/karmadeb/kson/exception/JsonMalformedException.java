package com.github.karmadeb.kson.exception;

/**
 * This exception is thrown when an
 * error occurs during json parsing
 */
public class JsonMalformedException extends JsonParseException {

    /**
     * Create the exception
     *
     * @param message the exception message
     */
    public JsonMalformedException(final String message) {
        super(String.format("Malformed json: %s", message));
    }
}
