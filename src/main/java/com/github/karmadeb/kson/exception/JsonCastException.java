package com.github.karmadeb.kson.exception;

import com.github.karmadeb.kson.element.JsonElement;

/**
 * This exception is thrown when a json
 * type is cast to an incompatible type
 */
public class JsonCastException extends RuntimeException {

    /**
     * Initialize the exception
     *
     * @param from the class converting from
     * @param to the class to convert to
     */
    public JsonCastException(final Class<? extends JsonElement> from, final Class<? extends JsonElement> to) {
        super("Cannot convert type " + from.getSimpleName() + " to " + to.getSimpleName());
    }
}