package com.github.karmadeb.kson.annotation.serialization;

import com.github.karmadeb.kson.reflection.ObjectTransformer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Represents a field which has an
 * especial transformer
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface SpecialTransformer {

    /**
     * Get the field transformer
     *
     * @return the field transformer
     */
    Class<? extends ObjectTransformer<?>> transformer();
}
