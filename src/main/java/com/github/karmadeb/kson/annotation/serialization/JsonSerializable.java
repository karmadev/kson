package com.github.karmadeb.kson.annotation.serialization;

import com.github.karmadeb.kson.annotation.deserialization.JsonConstructor;
import com.github.karmadeb.kson.reflection.JsonSerializer;
import com.github.karmadeb.kson.reflection.serialization.JsonClassSerializer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.github.karmadeb.kson.reflection.transformer.TransformerUtils;

/**
 * Represents a class which fields can
 * be serialized into a json object
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface JsonSerializable {

    /**
     * Get the serializer serial. The serial
     * is used later by {@link JsonConstructor}
     *
     * @return the serializable constructor
     * version
     */
    long serialId();

    /**
     * Get if the serializer should serialize
     * this object always as a simple object.
     * This is useful if you don't want to run
     * method {@link TransformerUtils#simpleTransform(Object)}, but do that
     * automatically when running {@link TransformerUtils#transform(Object)}.
     * This also allows to define a full transform with
     * a child object which will transformed as a
     * simple transformed object. This option is not
     * heritable, so child elements won't have the
     * same value as their parents
     *
     * @return if the serialized output should
     * be always simple for this object only
     */
    boolean simpleOutput() default false;

    /**
     * Get if the serializer should include
     * null elements
     *
     * @return if the serializer should also
     * include null elements
     */
    boolean serializeNulls() default true;

    /**
     * Get if the serializer serializes only
     * the public class fields
     *
     * @return if the serializer serializes only
     * the public fields
     */
    boolean publicFields() default true;

    /**
     * Get if the serializer serializes only
     * the {@link JsonInclude} fields only. This option
     * conflicts with {@link #publicFields()}, which
     * means if public fields is true, and this option
     * is also true, this option will revert back to false.
     * Alternatively if both values are false, all fields will
     * be accepted unless {@link JsonExclude} is provided
     *
     * @return if the serializer serializes only
     * the fields declared with {@link JsonInclude}
     */
    boolean explicitlyIncludedOnly() default false;

    /**
     * Get the serializer used to
     * serialize this object
     *
     * @return the object serializer
     */
    Class<? extends JsonSerializer<?>> serializer() default JsonClassSerializer.class;
}
