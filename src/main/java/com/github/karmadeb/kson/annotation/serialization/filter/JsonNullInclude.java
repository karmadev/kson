package com.github.karmadeb.kson.annotation.serialization.filter;

import com.github.karmadeb.kson.annotation.serialization.JsonSerializable;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Represents a field in a {@link JsonSerializable}
 * which must be included in the serialization process
 * depending on if its value is null or not
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface JsonNullInclude {

    /**
     * Get if the field should be included
     * if its null
     *
     * @return if the field should be included
     * if null
     */
    boolean include() default true;
}