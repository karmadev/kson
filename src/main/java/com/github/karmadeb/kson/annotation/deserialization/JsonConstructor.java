package com.github.karmadeb.kson.annotation.deserialization;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Represents a method which is used
 * by the json deserializer to load
 * a serialized json object
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.CONSTRUCTOR)
public @interface JsonConstructor {

    /**
     * Get the constructor serial version
     *
     * @return the serial version of the
     * constructor
     */
    long serialId();
}
