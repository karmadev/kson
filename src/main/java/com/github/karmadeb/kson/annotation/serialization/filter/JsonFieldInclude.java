package com.github.karmadeb.kson.annotation.serialization.filter;

import com.github.karmadeb.kson.annotation.serialization.JsonSerializable;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Represents a field in a {@link JsonSerializable}
 * which when included, instead of including the whole
 * object, only a field of the field is included
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface JsonFieldInclude {

    /**
     * Get the object field to include
     *
     * @return the field name of the object
     */
    String field();
}
