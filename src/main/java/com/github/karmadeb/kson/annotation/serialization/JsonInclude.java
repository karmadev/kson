package com.github.karmadeb.kson.annotation.serialization;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Represents a field in a {@link JsonSerializable}
 * which must be included in the serialization process
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface JsonInclude {

    /**
     * Get the field name in the
     * serialized object
     *
     * @return the field name
     */
    String name() default "%default%";
}