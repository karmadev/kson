package com.github.karmadeb.kson.annotation.deserialization;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Represents a parameter which must
 * be read from a serialized json field
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface JsonParameter {

    /**
     * Get the field name to read
     * from
     *
     * @return the field name
     */
    String fieldName();

    /**
     * Get the expected type of the
     * object
     *
     * @return the expected object type
     */
    Class<?> expectedType() default Object.class;
}